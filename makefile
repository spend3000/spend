.RECIPEPREFIX = >

install:
> pip install .

dev:
> pip install -e .[dev]

test:
> zuluCrypt-cli --test && pytest spend/test

apidoc:
> sphinx-apidoc -f -o docs/source ./spend ./spend/test