**Super Portable ENcryption Device**

.. warning::
    This project is a prototype. It has some known flaws, and can contain various other bugs. Encryption process have not been audited so
    DO NOT USE IT IN RISKY SITUATIONS !

Spend is a mobile device which allows you to copy and transfer files among flash storages such as USB key, SD cards...
It can also encrypt transfered files, using veracrpyt containers for files and PGP to interact with you

.. image:: https://i.imgur.com/bxCbJ4U.jpg

This repository contains the main python package, designed to run on an `OrangePi Zero <https://www.orangepi.org>`_ with a Oled screen and 3 push buttons connected via GPIO.

.. note::
    v 0.0 has not been released yet, which means that is code is still under development and may not run directly.
    Updates soon

**Project documentation can be found** `here at readthedocs <http://spend.readthedocs.io/>`_
Repository `there at Gitlab <https://gitlab.com/spend3000/spend/>`_

Contact : spend3000 [at] protonmail.com