#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import logging
from glob import glob
from os import environ, remove
from os import path
from os.path import exists
from shutil import rmtree

from configobj import ConfigObj

from spend.display.Message import Message
from spend.display.init import start_display_management, teardown_display_management
from spend.exceptions import UpdateException, DisplayableException
from spend.helpers import get_config

conf = None


class SpendUpdater:
    """Spend self update manager"""

    update_directory = ''
    debug = False
    test = False

    def __init__(self, debug=False, test=False):
        self.debug = debug
        self.test = test

    def run(self):
        """Run update procedure"""
        self.update_directory = path.expanduser(path.join('~', 'update'))

        new_gpg_key = self._check_gpg_update()
        if new_gpg_key:
            self._update_gpg_recipient(new_gpg_key)
            remove(new_gpg_key)

        if exists(path.join(self.update_directory, 'update.sh')):
            import subprocess
            subprocess.call(path.join(self.update_directory, 'update.sh'), shell=True)

        if path.isdir(path.join(self.update_directory, 'spend')):
            self._update_spend()

    def _check_gpg_update(self):
        """Return True if *one* file with .gpg extension is found in
        self.update_directory, False otherwise"""
        files = glob(path.join(
            self.update_directory,
            '*.gpg'))

        logging.debug(files)

        if len(files) > 1:

            logging.error("Update Gpg : Too many gpg files detected")
            logging.error(Message(template=Message.TEMPLATE_TYPE_ERROR, content=["Update Gpg", "Too many files"]))

        elif len(files):
            return files.pop()

        return False

    def _update_gpg_recipient(self, keypath):
        """Modify gpg recipient used by spend :
        - delete old gpg recipient
        - import new public key
        - modify spend conf to use new recipient
        - remove supplied key file"""
        gpg = self._init_gnupg()

        cur_fingapt = conf['VOLUME']['recipient']

        logging.info(Message(template=Message.TEMPLATE_TYPE_SPLASH, content=['Update', 'Gpg'], duration=1.5))
        logging.info('Gonna delete key %s', cur_fingapt)

        if not self.test:
            gpg.delete_keys(cur_fingapt)
            res = self._import_key_from_file(gpg, keypath)

            new_fingapt = res.fingerprints[0]
            logging.info("Setting recipient to new fingerprint : %s", new_fingapt)
            logging.info(Message(content=['fingapt : {!s}'.format(new_fingapt)], duration=3))

            conf['VOLUME']['recipient'] = new_fingapt

            custom_config_files = glob(path.expanduser(path.join('~', '.spend', '*.ini')))

            for _path in custom_config_files:
                logging.info('Gonna update config %s', _path)

                _conf = ConfigObj(_path)
                if not 'VOLUME' in _conf.keys():
                    _conf['VOLUME'] = {'recipient': new_fingapt}
                else:
                    _conf['VOLUME']['recipient'] = new_fingapt

                with open(_path, 'br+') as configfile:
                    _conf.write(configfile)

            logging.info('Check update success (path:%s)', ', '.join(custom_config_files))

    def _update_spend(self):
        """Triggers Spend package update using pip3"""
        # noinspection PyUnresolvedReferences
        from sh import pip3

        path_name = path.join(self.update_directory, 'spend')
        logging.info(Message(template=Message.TEMPLATE_TYPE_SPLASH, content=['Software', 'update...']))

        res = pip3.install(
            '--upgrade-strategy',
            'only-if-needed',
            path_name,
            _out='/var/log/spend/install.log'
        )

        logging.debug("Spend update intent returned : %s", res)
        rmtree(path_name)

    @staticmethod
    def _import_key_from_file(gpg, keypath):
        """Import a gpg public key contained in file : keypath argument"""
        with open(keypath) as f:
            key = f.read()
            import_res = gpg.import_keys(key)

            if import_res.count != 1:
                exc = UpdateException()
                exc._message_content[0] += ' : %s' % import_res.results[0]['text']
                exc._error_code = 'UP10'
                raise exc

            logging.info("1 key imported")
            return import_res

    @staticmethod
    def _init_gnupg():
        """Create a Gpg instance with gnupghome=~/.gnupg and returns it"""
        import gnupg

        gnupghome = path.expanduser("~") + '/.gnupg'

        logging.debug("Init gpg on gnupghome=%s", gnupghome)
        gpg = gnupg.GPG(gnupghome=gnupghome)
        return gpg


def main():
    # Command line options and help
    argparser = argparse.ArgumentParser(description='Spend update script')
    argparser.add_argument('-t', '--test', action='store_true',
                           help='Simulate run', required=False)
    argparser.add_argument('-d', '--debug', action='store_true',
                           help='Set log level to DEBUG', required=False)

    args = argparser.parse_args()

    logging.basicConfig(
        level=(logging.DEBUG if args.debug else logging.INFO),
        format='[%(levelname)s] %(filename)s %(message)s')

    env = environ["SPEND_ENV"]
    if env not in ['test', 'prod']:
        raise RuntimeError('Bad value for SPEND_ENV variable')

    global conf
    conf = get_config(env + '.ini')

    display_enabled = conf['DEFAULT'].as_bool('display')
    Display = {'queue_manager': None, 'message_queue': None}

    if display_enabled:
        logging.info('Enabling display')
        Display = start_display_management()

    print("Starting Spend update script")
    try:
        s = SpendUpdater(args.debug, args.test)
        s.run()

    except (DisplayableException, Exception) as e:
        if issubclass(type(e), DisplayableException):
            print('Gonna display exception')
            message = e.as_message()
            message.is_last_one = True
            logging.error(message)
        else:
            # Issue a generic error message
            message = Message(template=Message.TEMPLATE_TYPE_ERROR, content=['Fatal error', str(type(e))],
                              duration=5)
            message.is_last_one = True
            logging.error(message)

        # raise e

    logging.info(Message(content=['Run done'], is_last_one=True))

    if display_enabled:
        teardown_display_management(Display)
