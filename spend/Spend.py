# -*- coding: utf-8 -*-

import logging
import os
from os import makedirs
from os.path import join, exists
from time import sleep

from spend.Volume import Volume
from spend.display.Message import Message
from spend.exceptions import NoTargetsFoundException, DisplayableException
from spend.helpers import copy_files, get_random_str, shred_file, read_file_as_bool
from spend.service.DecisionMaker import DecisionMaker
from spend.service.DeviceInspector import DeviceInspector
from spend.service.DeviceScanner import DeviceScanner


class Spend:
    """Main class.
    """

    def start(self, conf):
        """Spend main method

        Parameters
        ----------
        conf: ConfigParser.ConfigParser
            configuration

        Returns
        -------
        int
            Return code

        """
        logging.debug('\n\n------- Starting activities --------')

        try:

            inspector = DeviceInspector()
            scanner = DeviceScanner(conf, inspector)
            brain = DecisionMaker()

            devices = scanner.run()

            logging.debug('----Device inspection----')
            logging.info(Message(content=['inspecting', 'devices']))

            for d in devices:
                inspector.inspect(d, conf['DEVICES']['seeked_files'])

            logging.debug('------Device choice---------')

            encryption_state = read_file_as_bool(conf['DEFAULT']['crypt_state_file'])

            choice = brain.choose(devices, encryption_state)
            logging.info(Message(["In : %s" % choice['input_device'].get('ID_MODEL'),
                                  "Out : %s" % choice['output_device'].get('ID_MODEL')], duration=2))

            # encryption enabled
            if encryption_state:
                logging.info(Message(template=Message.TEMPLATE_TYPE_SPLASH, content=['Encryption', 'on'], duration=.75))
                volume = self.create_volume(conf,
                                            choice['output_device'],
                                            round(float(choice['input_device'].metadata['targets_size'])) + 1)
                volume.import_files(choice['input_device'].metadata['targets'])

            # Simple copy/transfer.
            # Using "output" directory in order to be able to skip copied files on a next scan
            else:
                output_directory = join(choice['output_device'].metadata['mount_point'], 'output')

                if not exists(output_directory):
                    makedirs(output_directory)

                logging.info(
                    Message(content=['Copying {!s} files'.format(len(choice['input_device'].metadata['targets']))]))

                copy_files(choice['input_device'].metadata['targets'], output_directory)

            shred_state = read_file_as_bool(conf['DEFAULT']['shred_state_file'])

            if shred_state:

                logging.info(Message(content=['Removing original files'], level=Message.LEVEL_WARNING))

                for file in choice['input_device'].metadata['targets']:
                    logging.debug('Shreding file %s', file)
                    shred_file(file)

            logging.info(Message(["Success"], template=Message.TEMPLATE_TYPE_SPLASH, duration=10, is_last_one=True,
                                 level=Message.LEVEL_WARNING))

            return 92

        except NoTargetsFoundException:
            logging.warning(
                Message(['No device with targets found', 'NOT01'], template=Message.TEMPLATE_TYPE_ERROR, duration=3,
                        level=Message.LEVEL_ERROR))
            return 92

        # TODO: Hard unmount ? test while writing ?
        except (DisplayableException, Exception) as e:
            if issubclass(type(e), DisplayableException):
                print('Gonna display exception')
                message = e.as_message()
                message.is_last_one = True
                logging.error(message)

                # let user enough time to actually read the message
                if e.get_duration():
                    sleep(e.get_duration())

            else:
                # Issue a generic error message
                message = Message(template=Message.TEMPLATE_TYPE_ERROR, content=['Fatal error', str(type(e))],
                                  duration=5)
                message.is_last_one = True
                logging.error(message)

            raise e

    # noinspection PyMethodMayBeStatic
    def create_volume(self, conf, device, size):
        """Trigger veracrypt volume creation.

        Parameters
        ----------
        conf : ConfigParser.ConfigParser
            Application configuration
        device: spend.Device
        size: int
            in Bytes

        Returns
        -------
        Volume
        """
        logging.info("Encrypted volume creation triggered")

        volume_name = '%s.files' % get_random_str(12)
        volume_path = os.path.join(device.metadata['mount_point'], volume_name)
        volume_mntpoint = volume_name

        vol = Volume(volume_path, volume_mntpoint, conf)
        vol.create(size / 1000000)

        return vol
