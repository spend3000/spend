# -*- coding: utf-8 -*-

from pyudev.device import Device as UdevDevice


class Device(UdevDevice):
    """Physical Device abstraction

    Extends ```pyudev.device.Device``` (```Mapping```) in order to store computed metadata in the object itself.
    """

    # Constants used in decision-making process
    DEVICE_TYPE_SD: str = "sd"
    DEVICE_TYPE_USB: str = "usb"
    DEVICE_TYPE_UNKNOWN: str = "?"

    def __init__(self, context, pyudev_device, mount_point=None):
        super(Device, self).__init__(context=context, _device=pyudev_device)

        self._metadata = {
            'device_type': Device.DEVICE_TYPE_UNKNOWN,
            'mount_point': mount_point,
            'available_space': 0,
            'targets': None,
            'targets_size': 0,
            'writable': False
        }

    @property
    def metadata(self):
        return self._metadata

    def __repr__(self):
        """Human Readable representation."""
        return '<Device> {!s} {!s} {!s}'.format(hex(id(self)), self.metadata['mount_point'], self.metadata['targets'])
