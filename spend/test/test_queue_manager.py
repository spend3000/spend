from queue import PriorityQueue

# noinspection PyPackageRequirements
import pytest

from spend.display.QueueManager import QueueManager
from spend.test.mocks.mock_message_producer import ProducerDuration, ProducerOnce, ProducerWarning


@pytest.fixture()
def pqueue():
    return PriorityQueue()


@pytest.fixture(scope="module")
def handler():
    class MockHandler:
        @staticmethod
        def handle(message):
            print('\nReceived : %s' % message)

        @staticmethod
        def clear():
            pass

    return MockHandler


@pytest.fixture()
def queue_manager(pqueue, handler):
    # manager = QueueManager(pqueue, MessageHandler)
    manager = QueueManager(pqueue, handler)
    manager.start()

    return manager


def test_single_message(show_running, pqueue, queue_manager):
    show_running('test_single_message')

    _run_test(ProducerOnce(pqueue), queue_manager)


def test_duration(show_running, pqueue, queue_manager):
    show_running('test_duration')

    _run_test(ProducerDuration(pqueue), queue_manager)


def test_warning_takeover(show_running, pqueue, queue_manager):

    show_running('test_warning_takeover',
                 'Warning message should overtake start message for its lifetime, an then let place to the first one')

    _run_test(ProducerWarning(pqueue), queue_manager)


def _run_test(producer_thread, queue_manager):
    producer_thread.start()

    # Test is completed when processes return
    producer_thread.join()
    queue_manager.join()
