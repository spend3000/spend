# noinspection PyPackageRequirements
import pytest

from spend.Spend import Spend as SpendClass
from spend.helpers import get_config

"""
Global fixtures
"""


@pytest.fixture(scope="module")
def conf():
    _conf = get_config('test.ini')
    return _conf


@pytest.fixture(scope="module")
def Spend():
    return SpendClass()


@pytest.fixture(scope="module")
def show_running():
    def _show_running(name, description=None):
        print('\n------------------------------\n**** TEST : %s ****' % name, end='')
        if description:
            print('\n%s' % description, end='')
        print('\n------------------------------\n')

    return _show_running
