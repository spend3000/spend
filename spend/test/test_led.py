import fcntl
import logging
from os import setgroups
from time import sleep

import OPi.GPIO as GPIO
# noinspection PyPackageRequirements
import pytest

from spend.service.LedManager import LedManager

logging.basicConfig(level=logging.DEBUG)


def _deactivate_file(file):
    with open(file, 'w+') as fd:
        fcntl.flock(fd, fcntl.LOCK_EX)
        fd.write('0')
        fcntl.flock(fd, fcntl.LOCK_UN)
        fd.close()


def _activate_file(file):
    with open(file, 'w+') as fd:
        fcntl.flock(fd, fcntl.LOCK_EX)
        fd.write('1')
        fcntl.flock(fd, fcntl.LOCK_UN)
        fd.close()


def _test_led(lock_file, manager):
    for i in [0, 1]:
        _activate_file(lock_file)
        manager.poll()
        sleep(TestLed.SLEEP_TIME)

        _deactivate_file(lock_file)
        manager.poll()
        sleep(TestLed.SLEEP_TIME)


@pytest.fixture(scope="class")
def manager(conf):
    setgroups([115])
    manager = LedManager(conf)
    GPIO.setmode(GPIO.BOARD)
    manager.init_gpio()

    yield manager

    manager.clean()


class TestLed:
    SLEEP_TIME = 0.5

    def test_led_shred(self, conf, manager):
        log = logging.getLogger('test_led_shred')
        log.debug('LED corresponding to shred should blink  2 times')

        lock_file = conf['DEFAULT']['shred_state_file']
        _test_led(lock_file, manager)

    def test_led_crypt(self, conf, manager):
        log = logging.getLogger('test_led_crypt')
        log.debug('LED corresponding to crypt should blink  2 times')

        lock_file = conf['DEFAULT']['crypt_state_file']
        _test_led(lock_file, manager)

    def test_concurrent_access(self, conf, manager):
        file = conf['DEFAULT']['crypt_state_file']

        with pytest.raises(IOError):
            with open(file, 'w+') as fd:
                fcntl.flock(fd, fcntl.LOCK_EX)

                # Try to read while file is locked
                value = manager.read_state(file)
                print('Received value')
                print(value)

                fcntl.flock(fd, fcntl.LOCK_UN)
                fd.close()

            if value is False:
                pass
            else:
                # noinspection PyUnresolvedReferences
                pytest.fail('Led manager returned something while read file was locked', False)
