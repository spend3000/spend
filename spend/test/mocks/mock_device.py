from .mock_device_metadata import MockDeviceMetadata


class MockDevice:
    metadata = None

    def __init__(self):
        self.metadata = MockDeviceMetadata
