import threading
from time import sleep

from spend.display.Message import Message


class Producer(threading.Thread):
    MESSAGE_SIMPLE = Message(content=['Test simple '])
    MESSAGE_3_SECS = Message(content=['Long one'], duration=2)
    MESSAGE_WARNING = Message(content=['Warning !', 'xxxx'], duration=3, level=Message.LEVEL_WARNING,
                              template=Message.TEMPLATE_TYPE_ERROR)
    MESSAGE_DONE = Message(content=[], is_last_one=True)

    MESSAGE_INTERVAL = 1.5

    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.queue = queue

    def _run(self, messages, sleep_time=MESSAGE_INTERVAL):
        i = 0
        for message in messages:
            self.queue.put(message)
            print('%s put to queue' % message.content)

            if i != 0:
                print('Sleeping for %d secs' % sleep_time)
                sleep(sleep_time)
            i += 1

        self.queue.put(Producer.MESSAGE_DONE)
        print('%s put to queue' % Producer.MESSAGE_DONE.content)

        self.queue.join()


class ProducerOnce(Producer):
    def run(self):
        self._run([Producer.MESSAGE_SIMPLE])


class ProducerDuration(Producer):
    def run(self):
        self._run([Producer.MESSAGE_3_SECS])


class ProducerWarning(Producer):
    def run(self):
        self._run([Producer.MESSAGE_SIMPLE, Producer.MESSAGE_WARNING], 6)
