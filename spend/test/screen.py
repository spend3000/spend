"""
Display message of different types
"""
if __name__ == '__main__':
    from time import sleep

    from spend.display.Message import Message
    from spend.display.MessageHandler import MessageHandler

    print('You can scan for i2c devices with the command : i2cdetect -r 0')

    messageHandler = MessageHandler()
    message = Message()

    data = [
        (Message.TEMPLATE_TYPE_NORMAL, ['Found 2 devices : 0.1/4G, 3.2/8G']),
        (Message.TEMPLATE_TYPE_SPLASH, ['Spend', 'v0.0.0']),
        (Message.TEMPLATE_TYPE_ERROR, ['GPG recipient not found', 'EGPGRNF']),
    ]

    for test in data:

        message.template = test[0]
        message.content = test[1]

        messageHandler.handle(message)

        sleep(5)

    messageHandler.clear()
