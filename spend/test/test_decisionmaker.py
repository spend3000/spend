import logging
from copy import copy

# noinspection PyPackageRequirements
import pytest

from spend.exceptions import DeviceNotWritableException, MoreThanTwoDevicesException, SameDeviceAndNoEncryptionException
from spend.exceptions import NoDeviceFoundException
from spend.exceptions import NoTargetsFoundException
from spend.exceptions import NotEnoughtSpaceException
from spend.service.DecisionMaker import DecisionMaker
from .mocks.mock_device import MockDevice
from .mocks.mock_device_metadata import MockDeviceMetadata


def _create_device_metadata(available_space=0, targets=None, targets_size=0, is_writable=None):
    if targets is None:
        targets = []

    metadata = MockDeviceMetadata
    metadata['available_space'] = available_space
    metadata['targets'] = targets
    metadata['targets_size'] = targets_size
    metadata['writable'] = is_writable

    # References are messing everything up
    return copy(metadata)


def _get_device(metadata):
    d = MockDevice()
    d.metadata = metadata

    return d


# noinspection PyUnresolvedReferences
@pytest.fixture(scope="module")
def decision_maker():
    return DecisionMaker()


@pytest.fixture
def device_empty():
    return _get_device(_create_device_metadata(available_space=2000, is_writable=True))


@pytest.fixture
def device_with_pictures():
    def _device_with_pictures(enought_space):
        return _get_device(_create_device_metadata(
            targets=['truc.bidule', 'test.machin'],
            targets_size=1050,
            available_space=2000 if enought_space else 500,
            is_writable=True
        ))

    return _device_with_pictures


@pytest.fixture
def device_readonly():
    return _get_device(_create_device_metadata(targets=['truc.bidule', 'test.machin'], is_writable=False))


logging.basicConfig(level=logging.DEBUG)


class TestDecisionMaker:
    """
    Choice| Subject
    0       No devices
    1       Three devices
    2       One device readonly
    3       One empty device
    4b      One device with pictures and enought available space, encryption off
    4b      One device with pictures and enought available space, encryption on
    5       One device with pictures and NOT enought available space
    6       Two devices with pictures
    7       One device with pictures and one empty
    """

    def test_choice_0(self, decision_maker):
        with pytest.raises(NoDeviceFoundException):
            decision_maker.choose([], is_encryption_active=False)

    def test_choice_1(self, decision_maker, device_empty):
        with pytest.raises(NotImplementedError):
            decision_maker.choose([device_empty, device_empty, device_empty], is_encryption_active=False)

    def test_choice_2(self, decision_maker, device_readonly):
        with pytest.raises(DeviceNotWritableException):
            decision_maker.choose([device_readonly], is_encryption_active=False)

    def test_choice_3(self, decision_maker, device_empty):
        with pytest.raises(NoTargetsFoundException):
            decision_maker.choose([device_empty], is_encryption_active=False)

    def test_choice_4(self, decision_maker, device_with_pictures):
        with pytest.raises(SameDeviceAndNoEncryptionException):
            decision_maker.choose([device_with_pictures(enought_space=True)], is_encryption_active=False)

    def test_choice_4b(self, decision_maker, device_with_pictures):
        d = device_with_pictures(enought_space=True)
        assert decision_maker.choose([d], is_encryption_active=True) == {
            'input_device': d, 'output_device': d}

    def test_choice_5(self, decision_maker, device_with_pictures):
        with pytest.raises(NotEnoughtSpaceException):
            decision_maker.choose([device_with_pictures(enought_space=False)], is_encryption_active=False)

    def test_choice_6(self, decision_maker, device_with_pictures):
        with pytest.raises(MoreThanTwoDevicesException):
            decision_maker.choose([device_with_pictures(enought_space=True), device_with_pictures(enought_space=True)],
                                  is_encryption_active=False)

    def test_choice_7(self, decision_maker, device_with_pictures, device_empty):
        pics = device_with_pictures(enought_space=False)

        assert decision_maker.choose([pics, device_empty], is_encryption_active=False) == {
            'input_device': pics, 'output_device': device_empty}
