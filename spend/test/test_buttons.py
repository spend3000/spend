import logging
from time import sleep

import OPi.GPIO as GPIO
# noinspection PyPackageRequirements
import pytest

from spend.service.ButtonManager import ButtonManager

logging.basicConfig(level=logging.DEBUG)


@pytest.fixture(scope='function')
def button_manager(conf):
    GPIO.setmode(GPIO.BOARD)
    ButtonManager.init_gpio()
    _manager = ButtonManager(conf)

    yield _manager

    _manager.clean()


def _read_file(file):
    with open(file, 'r') as fd:
        value = fd.read()
        fd.close()

    return value


class TestButtons:
    WAIT_FOR = 3

    # def test_shutdown_button(self, button_manager):
    #     button_manager.spawn(ButtonManager.TYPE_SHUTDOWN)
    #
    #     print('You now have %i seconds to press the button' % TestButtons.WAIT_FOR)
    #     sleep(TestButtons.WAIT_FOR)
    #
    #     button_manager.clean()

    def test_shred_button(self, conf, button_manager):
        file = conf['DEFAULT']['shred_state_file']
        self._test_button(file, button_manager, ButtonManager.TYPE_SHRED)

    def test_crypt_button(self, conf, button_manager):
        file = conf['DEFAULT']['crypt_state_file']
        self._test_button(file, button_manager, ButtonManager.TYPE_CRYPT)

    def _test_button(self, file, button_manager, type):
        initial_value = _read_file(file)

        button_manager.spawn(type)

        print('You now have %i seconds to press the button' % TestButtons.WAIT_FOR)
        sleep(TestButtons.WAIT_FOR)

        assert _read_file(file) not in [None, initial_value]

    def test_loop(self, button_manager):
        for i in range(0, 15):
            print('SW_CRYPT : %s' % ('HIGH' if GPIO.input(ButtonManager.SW_CRYPT) else 'LOW'))
            print('SW_SHRED : %s' % ('HIGH' if GPIO.input(ButtonManager.SW_SHRED) else 'LOW'))
            print('SW_SHUTDOWN : %s' % ('HIGH' if GPIO.input(ButtonManager.SW_SHUTDOWN) else 'LOW'))

            sleep(1)
