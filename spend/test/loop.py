import logging
import time

# noinspection PyPackageRequirements
import pytest

from spend.service.DecisionMaker import DecisionMaker
from spend.service.DeviceInspector import DeviceInspector
from spend.service.DeviceScanner import DeviceScanner

logging.basicConfig(level=logging.DEBUG)


class TestLoop:
    devices = None
    log = None

    def test_loop_start(self, conf, Spend):
        if not self.log:
            self.log = logging.getLogger('test loop')
            # logging.getLogger().setLevel(logging.INFO)

        choice = input("Type 'exit' to exit, press Enter otherwise\n")

        if "exit" == choice:
            return True

        time_slept = int(conf['TEST']['device_preparation_time'])
        print(
            "Plug your stuff within the waiting time of {!s} seconds, after start".format(time_slept))
        time.sleep(time_slept)

        self._process_test(Spend, conf)

        self.test_loop_start(conf, Spend)

    @pytest.mark.timeout(25)
    def _process_test(self, Spend, conf):
        print(
            "\nPlugg stuff as you want. Detection in {!s} seconds\n".format(6))
        time.sleep(6)

        inspector = DeviceInspector()
        brain = DecisionMaker()

        devices = self._get_devices(conf, inspector)

        for d in devices:
            d.metadata = inspector.inspect(d, conf['DEVICES']['seeked_files'])

        choice = brain.choose(devices, is_encryption_active=True)
        inp = choice['input_device']
        outp = choice['output_device']

        print(
            "A choice was made : \nInput : {!s}\nOutput{!s}".format(inp, outp))

    @pytest.mark.timeout(15)
    def _get_devices(self, conf, inspector):
        if not bool(self.devices):
            scanner = DeviceScanner(conf, inspector)
            self.devices = scanner.run()

        return self.devices
