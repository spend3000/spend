import filecmp
import logging
import os

# noinspection PyPackageRequirements
import pytest

from spend import helpers
from spend.Volume import Volume
from spend.exceptions import UnknownRecipientException


@pytest.fixture(scope="class")
def volume_factory():
    def _volume_factory(conf, tempdir):
        # volume_name = conf.get("TEST", 'filename')
        volume_name = conf["TEST"]['filename']
        # volume_mnt_prefix = conf.get('TEST', 'mount_prefix')
        volume_mnt_prefix = conf['TEST']['mount_prefix']
        volumepath = os.path.join(str(tempdir), volume_name)
        return Volume(volumepath, volume_mnt_prefix, conf)

    return _volume_factory


logging.basicConfig(level=logging.DEBUG)


class TestVolume:

    volume = None

    def test_unknown_recipient(self, conf, tmpdir, volume_factory):
        log = logging.getLogger('test_unknown_recipient')
        recipient = conf['VOLUME']['recipient']
        conf['VOLUME']['recipient'] = helpers.get_random_str(50)

        log.debug('test_unknown_recipient. recipient set to %s', conf['VOLUME']['recipient'])

        with pytest.raises(UnknownRecipientException):
            volume_factory(conf, tmpdir)

        conf['VOLUME']['recipient'] = recipient

    @pytest.mark.timeout(200)
    def test_write_volume(self, volume_factory, conf, tmpdir):
        log = logging.getLogger('test_write_volume')
        log.debug('test_write_volume')

        # Create local based random file
        locfile = os.path.join(str(tmpdir), conf['TEST']['test_file'])
        helpers.write_file(data=helpers.get_random_str(
            80), filepath=locfile)

        # Create and mount volume
        volume = volume_factory(conf, tmpdir)
        volume.create(int(conf['TEST']['volume_size']))

        volume.import_files([locfile])

        # Copy file
        # copyfile(locfile, volume.mount_point + '/' + os.path.basename(locfile))

        # Close and reopen volume
        # volume.dismount()

        volume.mount()

        # Compare files
        assert filecmp.cmp(locfile, volume.mount_point + '/' + os.path.basename(locfile))

        self._clean_tests(volume)

    def _clean_tests(self, volume):
        try:
            volume.dismount()
            os.remove(volume.filepath)
            # os.remove(self.volume.get_key_name())
            del self.volume
        except OSError:
            logging.info('Tried to clean unexistant file')
        except AttributeError as e:
            logging.error(e)
