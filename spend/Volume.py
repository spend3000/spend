# -*- coding: utf-8 -*-

import logging
from os import path
from subprocess import CalledProcessError
from time import time

# noinspection PyPackageRequirements
from gnupg import GPG
from math import ceil
# noinspection PyUnresolvedReferences
from sh import truncate, zuluCrypt_cli

from spend.display.Message import Message
from spend.exceptions import SymetricEncryptionException, AsymetricEncryptionException, UnknownRecipientException
from spend.helpers import write_file, get_random_str, copy_files


class Volume:
    """Zulucrypt volume abstraction."""

    CONF_SECTION = 'VOLUME'

    def __init__(self, filepath, mount_prefix, config):
        self.conf = config
        self._init_gnupg(config[self.CONF_SECTION]['gnupghome'])

        self.filepath = filepath
        self.mount_prefix = mount_prefix
        self.mount_point = path.join(config[self.CONF_SECTION]['mount_dir'], mount_prefix)
        self.volume_key = None

    def _init_gnupg(self, gnupghome):
        """Perform GPG check and store instance in the object

        Parameters
        ----------
        gnupghome : str
            GPG home directory

        Raises
        ------
        UnknownRecipientException
            GPG key present in config is not present in known keys
        """
        logging.debug("Init gpg on gnupghome=%s", gnupghome)
        gpg = GPG(gnupghome=gnupghome)

        known_fingerprints = gpg.list_keys().fingerprints

        if not self.conf[self.CONF_SECTION]['recipient'] in known_fingerprints:
            logging.critical("Gpg recipient %s not found in known_fingerprints (%s)",
                             self.conf[self.CONF_SECTION]['recipient'], str(known_fingerprints))
            raise UnknownRecipientException()

        self.gpg = gpg

    def create(self, size):
        """Trigger veracrypt volume creation

        Parameters
        ----------
        size : float
            Volume size in Mb
        """
        logging.info(Message(["Starting volume creation"], duration=1))

        if path.isfile(self.filepath):
            raise RuntimeError('Volume file already exists (path : {!s})'.format(self.filepath))

        # Key is stored in object in order to be able to open volume later
        self._get_key()

        # Volume under 3MB are not allowed
        size = ceil(size)
        size = 4 if size < 4 else size

        start = time()
        logging.info(Message(['Creating','%sMb volume' % str(size), 'It can be long...']))

        # Setup encryption options if defined in configuration
        config_encryption = self.conf[self.CONF_SECTION]['encryption']
        config_hash = self.conf[self.CONF_SECTION]['hash']
        ciphers = None

        if bool(config_encryption) or bool(config_hash):
            if not config_encryption:
                config_encryption = 'aes'
            if not config_hash:
                config_hash = 'ripemd160'

            logging.debug('Applying custom encryption parameters : {config_encryption} {config_hash}',
                          config_encryption=config_encryption, config_hash=config_hash)

            ciphers = '/dev/urandom.{encryption}.xts-plain64.256.{hash}'.format(encryption=config_encryption,
                                                                                hash=config_hash)

        try:
            # Create file of the required size
            logging.debug('Gonna truncate file %s', self.filepath)

            truncate_command = truncate('--size', str(size) + 'M', self.filepath, _bg=True)
            truncate_command.wait()
            # Other ways to achieve that.
            # Remains here as it takes a lot of time and needs to be improved
            # system('truncate --size {!s} {!s}'.format(str(size) + 'M', self.filepath))
            # call('truncate --size {!s} {!s}'.format(str(size) + 'M', self.filepath), shell=True)
            # f = open(self.filepath, "wb")
            # f.seek(1024 * 1024 * size - 1)
            # f.write(b"\0")
            # f.close()

            trucateend = time()
            logging.debug('file truncated in %s seconds', str(trucateend - start))
            logging.info(Message(['Encrypting','%sMb volume' % str(size), 'It can be long...']))

            if ciphers:
                zulucrypt_cli_create = zuluCrypt_cli.bake(g=ciphers)
            else:
                zulucrypt_cli_create = zuluCrypt_cli.bake()

            # seteuid(0)
            zulucrypt_cli_create(
                '-c',  # create an encrypted volume
                '-k',  # Do not ask questions
                '-d', self.filepath,  # Device path
                '-z', self.conf[self.CONF_SECTION]['filesystem'],  # Fs
                '-t', self.conf[self.CONF_SECTION]['type'],  # Volume type
                '-p', self.volume_key,
                _fg=True
            )

            # seteuid(1000)

            logging.info(Message(['Volume created'], duration=2))
            end = time()
            logging.debug('Container created in : %s seconds', str(end - start))

        except Exception as e:
            # set at debug as it may contains sensitive data
            logging.debug(e)
            raise SymetricEncryptionException()

    def mount(self):
        """Mount volume"""
        if not path.isfile(self.filepath):
            raise OSError("File doesn't exist")

        if not bool(self.volume_key):
            raise OSError("Volume key not defined")

        logging.info('Mounting volume %s', self.filepath)
        logging.info(Message(content=['Opening encrypted volume']))

        # seteuid(0)
        try:
            zuluCrypt_cli(
                '-o',
                '-d', self.filepath,
                '-m', self.mount_prefix,
                '-M',  # create a publicly accessible "mirror" of the mount point in "/run/media/public/"
                '-t', self.conf[self.CONF_SECTION]['type'],
                '-e', 'rw',
                '-p', self.volume_key
            )

            return True
        except Exception as e:
            logging.critical(e)
            logging.critical(type(e))
            raise

        # seteuid(1000)

    def dismount(self):
        """Dismount volume."""
        logging.info('Dismounting volume %s', self.filepath)

        # seteuid(0)
        try:
            zuluCrypt_cli(
                '-q',
                '-d', self.filepath
            )

        except Exception as e:
            logging.critical("Dismount failed : %s", e)
            raise e

        # seteuid(1000)

    def import_files(self, files):
        """"Mount, copy specified files with _copy_files and dismount

        Parameters
        ----------
        files : List[str]

        Raises
        ------
        CalledProcessError
            Error while mounting volume or copying files
        """

        logging.info('Starting file import')
        logging.debug(files)

        try:
            self.mount()
            # seteuid(1000)
            self._copy_files(files)
            # seteuid(0)
        except CalledProcessError as e:
            logging.critical(e)
            raise
        finally:
            self.dismount()

    def get_key_name(self):
        """Return volume key file path."""
        return '{!s}.key'.format(self.filepath)

    def _get_key(self):
        """
        Create, crypt and write down volume key.

        - Random key created and stored in the object
        - Asymetrically encrypted version written down

        Raises
        ------
        AsymetricEncryptionException
            Error while encrypting volume key
        """
        volume_key = get_random_str(63)
        encrypted_volume_key = self.gpg.encrypt(
            data=volume_key,
            recipients=self.conf[self.CONF_SECTION]['recipient'],
            always_trust=True)

        if not encrypted_volume_key.ok:
            logging.critical(encrypted_volume_key.status)
            raise AsymetricEncryptionException(encrypted_volume_key.status)

        write_file(self.get_key_name(), encrypted_volume_key)

        logging.debug('Volume key encrypted and stored under name : %s', self.get_key_name())
        self.volume_key = volume_key

    def _copy_files(self, files):
        """Check mountpoint and copy files there

        Parameters
        ----------
        files: Union[str, PathLike]

        Raises
        ------
        OsError
            Volume is not mounted
        """
        if not path.ismount(self.mount_point):
            raise OSError('Volume is not mounted')

        logging.info(Message(['importing files']))

        copy_files(files, self.mount_point)
