#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import logging
from logging import config as logging_config
from os import path, getpid, environ, kill
from signal import signal, SIGUSR1, SIGHUP, SIGALRM
from sys import exit

import OPi.GPIO as GPIO
from _signal import SIGTERM
from pkg_resources import resource_filename, Requirement

from spend.Spend import Spend
from spend._version import __version__
from spend.display.Message import Message
from spend.display.init import start_display_management, teardown_display_management, kill_display_management
from spend.helpers import get_config, umount_devices, get_child_processes
from spend.service.ButtonManager import ButtonManager
from spend.service.DeviceInspector import DeviceInspector
from spend.service.DeviceScanner import DeviceScanner
from spend.service.LedManager import LedManagerThread, LedManager

# Holds main config
spend_config = None
# Uses to access resources via pkg_resources's Requirement
requirement_name = 'spend=={!s}'.format(__version__)
# Flag set when hard shutdown is triggered
is_killing_process = False


def shutdown(signal_number, current_stack_frame):
    logging.debug('__main__ shudown called. Received signal %s', signal_number)
    global is_killing_process
    is_killing_process = True
    raise SystemExit('Manual shutdown')


def main():
    # Read mode from env variable
    return_code = 1
    env = environ["SPEND_ENV"]
    if env not in ['test', 'prod']:
        raise RuntimeError('Bad value for SPEND_ENV variable')

    print("*************\n** Spend **\n*************\nCopyright 2019 spend3000\nThis program comes with \
ABSOLUTELY NO WARRANTY; see licence for details.\n\nStarting standard run. Initialization...")

    # Init logging
    logging_config_file = resource_filename(Requirement.parse(requirement_name), path.join('spend', 'config', 'logging.ini'))
    spend_config:dict = get_config(env + '.ini')
    logging_config.fileConfig(logging_config_file)

    # Set log level according to env
    if env == 'prod':
        logging.getLogger().setLevel(logging.INFO)

    else:

        def pretty(d, indent = 2):
            for key, value in d.items():
                print('\t' * indent + str(key))
                if isinstance(value, dict):
                    pretty(value, indent+1)
                else:
                    print('\t' * (indent+1) + str(value))

        pretty(spend_config)

    # Read hardware support from config
    display_enabled = spend_config['DEFAULT'].as_bool('display')
    led_enabled = spend_config['DEFAULT'].as_bool('led')
    button_enabled = spend_config['DEFAULT'].as_bool('button')

    # Holds hardware-related instances
    led_manager = None
    button_manager = None
    Display = {}

    try:

        signal(SIGUSR1, shutdown)
        signal(SIGHUP, shutdown)
        # signal(SIGCHLD,shutdown)
        signal(SIGALRM, shutdown)

        if led_enabled or button_enabled:
            # Start GPIO management
            logging.debug('Starting GPIO management')
            GPIO.setmode(GPIO.BOARD)

        if display_enabled:
            Display = start_display_management()

        if led_enabled:
            LedManager.init_gpio()

            # Start LED management
            led_manager = LedManagerThread(LedManager(spend_config))
            led_manager.start()

        if button_enabled:
            # Start buttons management
            ButtonManager.init_gpio()
            button_manager = ButtonManager(spend_config)
            button_manager.spawn(ButtonManager.TYPE_SHUTDOWN, pid=getpid())
            button_manager.spawn(ButtonManager.TYPE_SHRED)
            button_manager.spawn(ButtonManager.TYPE_CRYPT)

        spend = Spend()

        # Start actual processing
        if 92 == spend.start(spend_config):
            print(".\n\nExiting...")
            logging.info(Message(['Exiting'], template=Message.TEMPLATE_TYPE_SPLASH, duration=2, is_last_one=True))
            return_code = 0

    except (KeyboardInterrupt, Exception, SystemExit) as e:

        if type(e) == KeyboardInterrupt:
            global is_killing_process
            is_killing_process = True

        if type(e) != SystemExit:
            return_code = 1
            logging.critical(
                "Exception received : [%s] %s\nUnmounting devices and exiting..", type(e), e)

            if spend_config['DEFAULT'].as_bool('log_stacktrace'):
                import traceback
                file_logger = logging.getLogger('rotating')
                file_logger.error(str(e))
                file_logger.error(traceback.format_exc())
        # raise e

    finally:
        # Teardown hardware
        if button_enabled:
            button_manager.clean()

        if led_enabled:
            led_manager.stop()
            led_manager.join()

        if display_enabled:
            if is_killing_process:
                kill_display_management(Display)
            else:
                teardown_display_management(Display)

        if led_enabled or button_enabled or display_enabled:
            GPIO.cleanup()

        # Stop activities
        for pid in get_child_processes(getpid()):
            _pid = int(pid.strip())
            logging.debug('gonna kill process %d', _pid)
            try:
                kill(_pid, SIGTERM)
            except ProcessLookupError:
                logging.warning('Tried to kill unexistent process %d', _pid)

        # Look for devices && unmount
        inspector = DeviceInspector()
        scanner = DeviceScanner(spend_config, inspector)
        umount_devices(scanner.get_devices_list())

    if env != 'test':
        # noinspection PyUnresolvedReferences
        from sh import systemctl
        systemctl('poweroff')

    exit(return_code)


if __name__ == '__main__':
    main()
