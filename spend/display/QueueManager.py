import logging
from builtins import list
from logging import LogRecord
from threading import Thread, Timer
from time import sleep
from typing import Union

from spend.display.Message import Message


class StopListeningException(BaseException):
    pass


class PreviousItemsList(list):
    """
    Old thing ?
    List that can have only one Message without duration
    On insert, previous elem is discarded
    """

    def __setitem__(self, key, value: Message):
        print('setitem')
        print(self)
        self[key] = value


class QueueManager(Thread):
    """
    This module  handles Message bus management.
    It allows Message prioritization and Temporary messages.
    """
    # Poll frequency in seconds
    CHECK_FREQUENCY = 1

    timers = dict()

    previous_items: PreviousItemsList = PreviousItemsList()
    current_item = None

    external_queue = None

    message_handler = None

    def __init__(self, queue, message_handler):

        Thread.__init__(self)
        self.external_queue = queue
        self.message_handler = message_handler

    def run(self):
        """Loop until StopListeningException is received

        Returns
        -------

        """
        while True:
            # print('\n\nQueueManager looping')

            try:
                self.poll()
            except StopListeningException:
                break

        self.stop()

    def poll(self):
        """Get a new item from the Queue and process it

        Returns
        -------

        """
        new_item = self.external_queue.get()
        # Check the delayed items first
        self.process_previous_items(new_item)

        # Blocks until a message arrive
        self.process_message(new_item)

    def process_previous_items(self, new_item=Message(level=Message.LEVEL_DEBUG)):
        """Check if previous messages can be displayed considering new Message.
        If last previous item can overtake new one and nothing is displayed, of if it can overtake both ; it can be used

        Returns
        -------

        """
        if not len(self.previous_items) > 0:
            return

        # print('process_previous_items. not empty : %s' % str(self.previous_items))

        last_previous_item = self.previous_items[len(self.previous_items) - 1]
        # print('last_previous_item : %s' % str(last_previous_item))

        if last_previous_item.can_overtake(new_item) and (
                self.current_item is None or last_previous_item.can_overtake(self.current_item)):
            # print('Last previous item can overtake current and new one')
            self.previous_items.remove(last_previous_item)
            self.display(last_previous_item)
        # else:
        # print('Last previous item can\'t be displayed now, back in queue !')

    def process_message(self, raw_message: Union[Message, LogRecord]):
        """Check if message can be displayed considering current item. Start Timer if needed and ask for display

        Parameters
        ----------
        raw_message: Union[Message, LogRecord]

        Returns
        -------

        Raises
        ------
        StopListeningException
            Used to stop the Manager. Triggered when receiving Message(is_last_one=True)
        """
        message = raw_message.msg if type(raw_message) == LogRecord else raw_message
        # print('process_message')
        # Preventing display if :
        #  - Message currently displayed have higher priority
        #  - Message currently displayed have same priority and fixed duration
        if self.current_item and not message.can_overtake(self.current_item):
            # print('New message cannot overtake current one, pushing it back to queue')

            self.external_queue.put(message)

            sleep(QueueManager.CHECK_FREQUENCY)
            self.external_queue.task_done()
            return

        # print('New message accepted')

        if message.duration:
            # Store currently displayed item as it may
            # need to be restore afterwards
            if self.current_item is not None:
                # print('Adding current item %s to previous_items' % self.current_item)
                self.previous_items.append(self.current_item)

            # Timer + callback
            # print('Duration is set, starting timer')

            # Create id based on message's attributes
            # Useless ?
            message_id = hash(message)
            self.timers[message_id] = Timer(message.duration, self.next, kwargs={'message': message})
            self.timers[message_id].start()

        # Everything is okay, gonna display message
        self.external_queue.task_done()
        self.display(message)

        if message.is_last_one:
            raise StopListeningException()

    def display(self, message: Message):
        """Store argument as currently displayed message and pass it to the Handler

        Parameters
        ----------
        message: Message

        Returns
        -------

        """

        self.current_item = message
        self.message_handler.handle(message)

    def next(self, message: Message):
        """function called by a Timer when it wakes up.
        Trigger update

        Parameters
        ----------
        message: Message

        Returns
        -------

        """
        # print('Next called by %s' % message)

        # print('previous item is')
        # print(self.previous_items)

        # print('current item is')
        # print(self.current_item)

        # callback called by a message that was taken over
        # Removing related things
        if message in self.previous_items:
            # print('Found myself in previous item.. Vanishing !')
            self.previous_items.remove(message)

        if self.current_item == message:
            # print('Found myself as current_item, gonna call next then')
            # Remove current message and poll for new ones
            self.current_item = None
            self.process_previous_items()

    def stop(self):
        """
        Wait for Messages with duration to complete and stop instance
        Returns
        -------

        """
        print('Stop QueueManager')
        # We won"t display old items
        self.previous_items.clear()

        # Stop inflow
        self._empty_queue(self.external_queue)

        # If timers are on we have to wait for them.

        del_list = []
        # if len(self.timers):
        # print('Waiting because timers is not empty')

        while len(self.timers):
            for t in self.timers:
                if not self.timers[t].is_alive():
                    del_list.append(t)

            print('.')
            for item in del_list:
                try:
                    del self.timers[item]
                except KeyError:
                    pass

            sleep(.1)

        self.current_item = None
        self.previous_items.clear()

    def kill(self):
        """
        kills instance without waiting
        Returns
        -------

        """
        # print('Kill QueueManager')

        for timer in self.timers:
            self.timers[timer].cancel()
        # print('+Cancel timers')

        self.current_item = None
        self.previous_items.clear()
        # print('+Wipe current and previous items')

        self._empty_queue(self.external_queue)
        # print('+Empty external queue')

        logging.warning(Message(content=['.'], template=Message.TEMPLATE_TYPE_SPLASH, duration=1, is_last_one=True))

    @staticmethod
    def _empty_queue(queue):
        while not queue.empty():
            queue.get()
            queue.task_done()
        queue.join()
