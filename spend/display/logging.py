"""
This file hold classes for a custom logger, preparing LogRecord containing a Message to be push in a PriorityQueue
Overrides in order to

- filter log record not meant to be displayed
- Put only Message instances in the queue

"""
import logging
from logging.handlers import QueueHandler
from queue import PriorityQueue

from spend.display.Message import Message




class DisplayFormatter(logging.Formatter):
    def format(self, record):
        """Does nothing"""
        # print('formatter : Gonna return %s ' % type(record.msg))
        return record.msg


class DisplayFilter(logging.Filter):
    def filter(self, r):
        """Filter non-Message LogRecords"""
        # print('filter : Gonna return %s ' % str(0 if type(r.msg) == Message else 1))
        return 1 if type(r.msg) == Message else 0


class DisplayQueueHandler(QueueHandler):
    def prepare(self, record):
        """Return actual content"""
        self.format(record)
        record.args = None
        record.exc_info = None

        return record.msg


def create_display_handler(queue: PriorityQueue):
    """Bootstrap handler and registers it

    Parameters
    ----------
    queue: PriorityQueue

    Returns
    -------

    """

    logger = logging.getLogger()

    queue_handler = DisplayQueueHandler(queue)

    queue_handler.setFormatter(DisplayFormatter())
    queue_handler.addFilter(DisplayFilter())

    logger.addHandler(queue_handler)

    return queue_handler
