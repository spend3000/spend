"""
Functions managing global oled communication environment : PriorityQueue joining QueueManager and a logging.Logger
"""
import logging


def start_display_management():
    """
    - Create PriorityQueue
    - Create logging Handler
    - Create QueueManager thread reading from the PriorityQueue

    Returns
    -------
    {'queue_manager': QueueManager, 'message_queue': PriorityQueue, 'handler': logging.Handler}
    """
    from queue import PriorityQueue

    from spend.display.MessageHandler import MessageHandler
    from spend.display.QueueManager import QueueManager
    from spend.display.logging import create_display_handler

    logging.debug('Starting display management')

    message_queue = PriorityQueue()

    handler = create_display_handler(message_queue)

    queue_manager = QueueManager(message_queue, MessageHandler)
    queue_manager.start()

    return {'queue_manager': queue_manager, 'message_queue': message_queue, 'handler': handler}


def kill_display_management(display_props):
    """Stop display management with kill flag


    Parameters
    ----------
    display_props

    Returns
    -------

    """
    logging.debug('kill_display_management called')
    return teardown_display_management(display_props, kill=True)


def teardown_display_management(display_props, kill=False):
    """Stops disaplys management. Join threads.

    Parameters
    ----------
    display_props: {'queue_manager': QueueManager, 'message_queue': PriorityQueue, 'handler': logging.Handler}
    kill: bool

    Returns
    -------
    """

    logging.debug('Tearing down display management')

    _queue_manager = display_props.get('queue_manager')
    _message_queue = display_props.get('message_queue')
    _handler = display_props.get('handler')

    # Kill the queuemanager
    # logging.info(Message(is_last_one=True))

    logging.debug('Waiting for queue manager to join...')
    if kill:
        _queue_manager.kill()
        _queue_manager.join()
    else:
        _queue_manager.join()

    _queue_manager.message_handler.clear()

    logging.debug('Joined')

    logging.getLogger().removeHandler(_handler)
    logging.debug('Remove handler form logger.')
