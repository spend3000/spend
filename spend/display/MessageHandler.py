# -*- coding: utf-8 -*-
import itertools
import logging
from os import path

from PIL import ImageFont
from oled.device import ssd1306
from oled.render import canvas
from pkg_resources import resource_stream

from spend.display.Message import Message
from spend.helpers import get_resource_requirement


class MessageHandler:
    """
    This class handle the communication of Messages to the OLED display.
    It uses Pillow to draw content, and passes it to ssd1306
    """
    FONT_FILE = "firasanslight.ttf"

    FONT_SIZE_SMALL = 10
    FONT_SIZE_NORMAL = 14
    FONT_SIZE_BIG = 20

    @staticmethod
    def font(size):
        """
        Parameters
        ----------
        size: int

        Returns
        -------
        FreeTypeFont
        """
        return ImageFont.truetype(
            resource_stream(get_resource_requirement(), path.join('spend', 'assets', MessageHandler.FONT_FILE)), size)

    @staticmethod
    def handle(message):
        """Main function

        Parameters
        ----------
        message: Message

        Returns
        -------

        """
        # print('\nMessage handler will display message : %s' % message.__str__())

        # noinspection PyArgumentList
        device = ssd1306(port=0, address=0x3C, width=128, height=64)

        MessageHandler.clear()

        try:
            if message.template == Message.TEMPLATE_TYPE_ERROR:
                MessageHandler.draw_error(device, message.content)

            elif message.template == Message.TEMPLATE_TYPE_NORMAL:
                MessageHandler.draw_normal(device, message.content)

            elif message.template == Message.TEMPLATE_TYPE_SPLASH:
                MessageHandler.draw_splash(device, message.content)

            else:
                raise RuntimeError('Template unknown')

        except ValueError as e:
            logging.error('Received value error %s', e)

    @staticmethod
    def clear():
        """
        Clear screen by drawing a big rectangle

        Returns
        -------

        """
        # noinspection PyArgumentList
        device = ssd1306(port=0, address=0x3C, width=128, height=64)

        with canvas(device) as draw:
            draw.rectangle([(0, 0), (device.width, device.height)])

    @staticmethod
    def draw_error(device, messages):
        """Draw error types message

        Parameters
        ----------
        device: ssd1306
        messages: str[]

        Returns
        -------

        """
        # print('draw_error called')

        if len(messages) < 2:
            raise ValueError('Message should be a list of min size 2')

        font_small = MessageHandler.font(MessageHandler.FONT_SIZE_SMALL)
        font_normal = MessageHandler.font(MessageHandler.FONT_SIZE_NORMAL)

        error_text = MessageHandler._resize(messages[0], device, font_normal)
        error_code = MessageHandler._resize(messages[1], device, font_small)

        with canvas(device) as draw:
            line = 0

            for (strings, font) in [
                (error_text, font_normal),
                (error_code, font_small)
            ]:

                for string in strings:
                    draw.text((0, MessageHandler.FONT_SIZE_NORMAL * line), string, font=font, fill=255)
                    line += 1

    @staticmethod
    def draw_normal(device, messages):
        """Draw normal message

            Parameters
            ----------
            device: ssd1306
            messages: str[]

            Returns
            -------

            """
        # print('draw_normal called')

        font_normal = MessageHandler.font(MessageHandler.FONT_SIZE_NORMAL)

        # Flatten arrays of string obtained from Resize
        resized_messages = list(
            itertools.chain.from_iterable([MessageHandler._resize(txt, device, font_normal) for txt in messages]))

        with canvas(device) as draw:
            line_height = MessageHandler.FONT_SIZE_NORMAL
            line = 0

            for str_messages in resized_messages:
                draw.text((0, line_height * line), str_messages, font=font_normal, fill=255)
                line += 1

    @staticmethod
    def draw_splash(device, messages):
        """Draw splash message

            Parameters
            ----------
            device: ssd1306
            messages: str[]

            Returns
            -------

            """
        # print('draw_splash called')

        font_big = MessageHandler.font(MessageHandler.FONT_SIZE_BIG)

        y_start = round((device.height - (len(messages) * MessageHandler.FONT_SIZE_BIG)) / 2)

        with canvas(device) as draw:
            line_height = MessageHandler.FONT_SIZE_BIG
            line = 0

            for str_messages in messages:
                # Center text
                x = (device.width - font_big.getsize(str_messages)[0]) / 2
                y = line_height * line + y_start

                draw.text((x, y), str_messages, font=font_big, fill=255)
                line += 1

    @staticmethod
    def _resize(txt, device, font):
        """Break words to be displayable on device

        Parameters
        ----------
        txt: str Text to resize
        device: ssd1306
        font: FreeTypeFont

        Returns
        -------
        str[]
        """
        text_dimensions = font.getsize(txt)

        if text_dimensions[0] > device.width or text_dimensions[1] > device.height:
            # print('\nWARNING: text too big')
            txt = MessageHandler.break_words(font, device, txt)

        return txt if isinstance(txt, list) else [txt]

    @staticmethod
    def break_words(font, device, messages):
        """Break words worker

        Parameters
        ----------
        font: FreeTypeFont
        device: ssd1306
        messages: str[]

        Returns
        -------
        str[]
        """

        def _break_word(_font, _device, _text):
            _i = len(_text)

            while _font.getsize(_text)[0] > device.width:
                _i = len(_text) - 1
                _text = _text[0:_i]

            return _text, _i

        text = messages
        content = list()

        while len(text):
            # print('\n\nBreak words : iterating.')

            _return = _break_word(font, device, text)

            content.append(_return[0])
            text = text[_return[1]:len(text)]

        return content
