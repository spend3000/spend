# -*- coding: utf-8 -*-
from time import time


class Message:
    """Displayed message representation.
    Comparable, Hashable, Human-readable"""

    LEVEL_DEBUG = 40
    LEVEL_INFO = 30
    LEVEL_WARNING = 20
    LEVEL_ERROR = 10
    LEVEL_CRITICAL = 0

    TEMPLATE_TYPE_ERROR = 3
    TEMPLATE_TYPE_NORMAL = 2
    TEMPLATE_TYPE_SPLASH = 1

    TEMPLATE_TYPES = [TEMPLATE_TYPE_SPLASH, TEMPLATE_TYPE_NORMAL, TEMPLATE_TYPE_ERROR]

    def __init__(self, content=None, level=LEVEL_INFO, duration=None, template=TEMPLATE_TYPE_NORMAL, is_last_one=False):
        if content is None:
            content = list()

        self.message_type = None
        self.duration = duration
        self.level = level
        self.template = template
        self.content = content
        self.creation_date = time()
        self.is_last_one = is_last_one

    def __repr__(self):
        return '{}: {} [{}] {} {}'.format(
            self.__class__.__name__, ' '.join(self.content), 'Lvl.%s' % self.level,
            '[%ss]' % self.duration if self.duration else '',
            '[last_one]' if self.is_last_one else ' '
        )

    def __lt__(self, other):
        """
        Compare instances of message by priority first, then by creation_date
        Parameters
        ----------
        other

        Returns
        -------

        """
        if isinstance(other, Message):
            # Sort by priority first
            if other.level != self.level:
                return self.level < other.level
            else:
                return self.creation_date < other.creation_date

        raise RuntimeError('Message can only be compared with another instance of Message')

    def __hash__(self):
        return hash(((key, (' '.join(value) if value is list else value)) for (key, value) in self.__dict__.items()))

    def can_overtake(self, message):
        """
        Check if message  in argument can overtake
        A message can overtake others with lower level, or with same level and no duration

        Parameters
        ----------
        message: Message
        Returns
        -------
        boolean can_overtake
        """
        return message.level > self.level or (message.level == self.level and not message.duration)
