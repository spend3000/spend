"""
This module abstracts communication from one or many scripts to an OLED screen connected via GPIO
It receive Message from a PriorityQueue and display them using a MessageHandler
It can be plugged to the logging module for convenient use
"""