# -*- coding: utf-8 -*-
from spend.display.Message import Message


class DisplayableException(Exception):
    """
    Base class for Exceptions which could be displayed on the OLED display
    """
    _message_content = []
    _error_code = 0
    _duration = None
    _level = None

    def __init__(self, *args, **kwargs):
        # noinspection PyArgumentList
        Exception.__init__(self, *args, **kwargs)

    def as_message(self) -> Message:
        """
        Get exception as Message

        Returns
        -------
        Message
        """
        content = self._message_content
        content.append(str(self._error_code))
        return Message(template=Message.TEMPLATE_TYPE_ERROR, content=content, duration=self._duration,
                       level=self._level)

    def get_duration(self):
        return self._duration


class UpdateException(DisplayableException):

    def __init__(self, *args, **kwargs):
        DisplayableException.__init__(self, *args, **kwargs)
        self._message_content = ['Update error']
        self._error_code = 'UP00'
        self._duration = 5
        self._level = Message.LEVEL_CRITICAL


class AsymetricEncryptionException(DisplayableException):
    """PGP file encryption returns something else than OK"""

    def __init__(self, *args, **kwargs):
        DisplayableException.__init__(self, *args, **kwargs)

        self._message_content = ['PGP encryption error']
        self._error_code = 'ENC50'
        self._duration = 5
        self._level = Message.LEVEL_CRITICAL


class SymetricEncryptionException(DisplayableException):
    """Error on volume creation
   """

    def __init__(self, *args, **kwargs):
        DisplayableException.__init__(self, *args, **kwargs)

        self._message_content = ['Encryption error']
        self._error_code = 'ENC20'
        self._duration = 5
        self._level = Message.LEVEL_CRITICAL


class DeviceNotMountedException(BaseException):
    pass


class DeviceNotWritableException(BaseException):
    pass


class GhostMountpointException(BaseException):
    pass


class NoDeviceFoundException(DisplayableException):
    """
    No (interesting) device was found by DeviceScanner
    """

    def __init__(self, *args, **kwargs):
        DisplayableException.__init__(self, *args, **kwargs)

        self._message_content = ['No devices found...']
        self._error_code = 'SCAN00'
        self._duration = 3
        self._level = Message.LEVEL_CRITICAL


class SameDeviceAndNoEncryptionException(DisplayableException):
    """
    Input and output have been set to the same device, and no encryption have been chosen.
    It fails as it does not makes sense to duplicate files
    """

    def __init__(self, *args, **kwargs):
        DisplayableException.__init__(self, *args, **kwargs)

        self._message_content = ['Add other device or choose encryption']
        self._error_code = 'CHOS10'
        self._duration = 5
        self._level = Message.LEVEL_CRITICAL


class NoTargetsFoundException(DisplayableException):
    """
    One or more devices have been found, but DeviceInspector did not returned any file to be copied
    """

    def __init__(self, *args, **kwargs):
        DisplayableException.__init__(self, *args, **kwargs)

        self._message_content = ['No targets found']
        self._error_code = 'CHOS20'
        self._level = Message.LEVEL_ERROR
        self._duration = 3


class NotEnoughtSpaceException(DisplayableException):
    """
    No device have enough space to store files selected by DeviceInspector
    """

    def __init__(self, *args, **kwargs):
        DisplayableException.__init__(self, *args, **kwargs)

        self._message_content = ['Not enought space']
        self._error_code = 'INSP60'
        self._level = Message.LEVEL_CRITICAL
        self._duration = 3


class UnknownRecipientException(DisplayableException):
    """
    PGP recipient specified in config is not present in PGP keyring
    """

    def __init__(self, *args, **kwargs):
        DisplayableException.__init__(self, *args, **kwargs)

        self._message_content = ['Error in pgp config']
        self._error_code = 'PGP10'
        self._duration = 3
        self._level = Message.LEVEL_CRITICAL


class MoreThanTwoDevicesException(DisplayableException):
    """
    More than 2 devices found
   """

    def __init__(self, *args, **kwargs):
        DisplayableException.__init__(self, *args, **kwargs)

        self._message_content = ['More than 2 devices not supported']
        self._error_code = 'SCAN2'
        self._duration = 3
        self._level = Message.LEVEL_CRITICAL


class TargetOnBothSidesException(DisplayableException):
    """
    Targets found in both devices
   """

    def __init__(self, *args, **kwargs):
        DisplayableException.__init__(self, *args, **kwargs)

        self._message_content = ['Target files on both sides']
        self._error_code = 'CHOS2'
        self._duration = 3
        self._level = Message.LEVEL_CRITICAL
