# -*- coding: utf-8 -*-
import fcntl
import filecmp
import logging
import os

from pkg_resources import Requirement, resource_stream
from spend._version import __version__


class safeutil:
    """
    This is a script designed to be "safe" drop-in replacements for the
    shutil move() and copyfile() functions.
    These functions are safe because they should never overwrite an
    existing file. In particular, if you try to move/copy to dst and
    there's already a file at dst, these functions will attempt to copy to
    a slightly different (but free) filename, to avoid accidental data loss.
    More background here: http://alexwlchan.net/2015/06/safer-file-copying/
    """

    @staticmethod
    def _increment_filename(filename, marker='-'):
        """
        Returns a generator that yields filenames with a counter. This counter
        is placed before the file extension, and incremented with every iteration.
        For example:
        f1 = increment_filename("myimage.jpeg")
        f1.next() # myimage-1.jpeg
        f1.next() # myimage-2.jpeg
        f1.next() # myimage-3.jpeg
        If the filename already contains a counter, then the existing counter is
        incremented on every iteration, rather than starting from 1.
        For example:
        f2 = increment_filename("myfile-3.doc")
        f2.next() # myfile-4.doc
        f2.next() # myfile-5.doc
        f2.next() # myfile-6.doc
        The default marker is an underscore, but you can use any string you like:
        f3 = increment_filename("mymovie.mp4", marker="_")
        f3.next() # mymovie_1.mp4
        f3.next() # mymovie_2.mp4
        f3.next() # mymovie_3.mp4
        Since the generator only increments an integer, it is practically unlimited
        and will never raise a StopIteration exception.
        """
        # First we split the filename into three parts:
        #
        #  1) a "base" - the part before the counter
        #  2) a "counter" - the integer which is incremented
        #  3) an "extension" - the file extension
        basename, fileext = os.path.splitext(filename)

        # Check if there's a counter in the filename already - if not, start a new
        # counter at 0.
        if marker not in basename:
            base = basename
            value = 0

        # If it looks like there might be a counter, then try to coerce it to an
        # integer to get its value. Otherwise, start with a new counter at 0.
        else:
            base, counter = basename.rsplit(marker, 1)

            try:
                value = int(counter)
            except ValueError:
                base = basename
                value = 0

        # The counter is just an integer, so we can increment it indefinitely.
        while True:
            if value == 0:
                value += 1
                yield filename
            value += 1
            yield '%s%s%d%s' % (base, marker, value, fileext)

    @staticmethod
    def copyfile(src, dst):
        """
        Copies a file from path src to path dst.
        If a file already exists at dst, it will not be overwritten, but:
        * If it is the same as the source file, do nothing
        * If it is different to the source file, pick a new name for the copy that is distinct and unused, then copy the file there.

        Returns the path to the copy.

        """
        if os.path.isdir(dst):
            dst = os.path.join(dst, os.path.basename(src))

        if os.path.isdir(src):
            raise ValueError('Only copy files, not directories')

        if not os.path.exists(src):
            raise ValueError('Source file does not exist: {}'.format(src))

        # Create a folder for dst if one does not already exist
        if not os.path.exists(os.path.dirname(dst)):
            os.makedirs(os.path.dirname(dst))

        # Keep trying to copy the file until it works
        while True:

            dst_gen = safeutil._increment_filename(dst)
            dst = next(dst_gen)

            # Check if there is a file at the destination location
            if os.path.exists(dst):

                # If the namesake is the same as the source file, then we don't
                # need to do anything else.
                if filecmp.cmp(src, dst):
                    return dst

            else:

                # If there is no file at the destination, then we attempt to write
                # to it. There is a risk of a race condition here: if a file
                # suddenly pops into existence after the `if os.path.exists()`
                # check, then writing to it risks overwriting this new file.
                #
                # We write by transferring bytes using os.open(). Using the O_EXCL
                # flag on the dst file descriptor will cause an OSError to be
                # raised if the file pops into existence; the O_EXLOCK stops
                # anybody else writing to the dst file while we're using it.
                try:
                    src_fd = os.open(src, os.O_RDONLY)
                    dst_fd = os.open(dst,
                                     os.O_WRONLY | os.O_EXCL | os.O_CREAT)

                    # Read 100 bytes at a time, and copy them from src to dst
                    while True:
                        data = os.read(src_fd, 100)
                        os.write(dst_fd, data)

                        # When there are no more bytes to read from the source
                        # file, 'data' will be an empty string
                        if not data:
                            break

                    os.close(src_fd)
                    os.close(dst_fd)

                    # If we get to this point, then the write has succeeded
                    return dst

                # An OSError errno 17 is what happens if a file pops into existence
                # at dst, so we print an error and try to copy to a new location.
                # Any other exception is unexpected and should be raised as normal.
                except OSError as e:
                    if e.errno != 17 or e.strerror != 'File exists':
                        raise
                    else:
                        print('Race condition: %s just popped into existence' % dst)

            # Copying to this destination path has been unsuccessful, so increment
            # the path and try again
            dst = next(dst_gen)

    @staticmethod
    def move(src, dst):
        """
        Moves a file from path src to path dst.
        If a file already exists at dst, it will not be overwritten, but:
        * If it is the same as the source file, do nothing
        * If it is different to the source file, pick a new name for the copy that is distinct and unused, then copy the file there.

        Returns the path to the new file.

        """
        dst = safeutil.copyfile(src, dst)
        os.remove(src)
        return dst


def copy_files(files, to):
    from os.path import join, basename

    if isinstance(files, str):
        raise ValueError('Expecting arr')

    for f in files:
        print('+ ', )
        safeutil.copyfile(f, join(to, basename(f)))


def write_file(filepath, data):
    """Write data to filepath. No exceptions catched.
    Parameters:
    -----------
    filepath: str File path
    data: str     Data to be written in the file
    """
    logging.debug("writing file %s", filepath)

    file = open(filepath, 'w+')
    file.write(str(data))
    file.close()


def get_random_str(size):
    """
    Return x chars belonging to letters and digits.

    Parameters
    ----------
    size: int Len of the returned string

    Returns
    -------

    """
    import random
    import string

    poll = string.ascii_letters + string.digits
    return ''.join(random.choice(poll) for i in range(size))


# noinspection PyUnresolvedReferences
def shred_file(filename):
    """Shred designed file

    Parameters
    ----------
    filename: str Path of the file to be shreded

    Returns
    -------

    """
    from sh import shred

    logging.info("shreding file %s", filename)

    try:
        shred("-n1", "-f", "-u", "-z", "-x", filename)
    except sh.ErrorReturnCode_1 as e:
        logging.error("Error trying to shred file. Received : %s", e)


def get_config(filename='prod.ini'):
    """
    Return ConfigObj
    Parameters
    ----------
    filename: str Config filename (e.g test.ini)

    Returns
    -------
    ConfigObj
    """
    from configobj import ConfigObj
    from os import path
    # noinspection PyUnresolvedReferences
    from configobj.validate import Validator

    requirement = get_resource_requirement()

    _conf = ConfigObj(

        resource_stream(requirement, path.join('spend', 'config', filename)),
        configspec=resource_stream(requirement, path.join('spend', 'config', 'configspec.ini')),
        stringify=True, file_error=True)

    # logging_conf = ConfigObj(resource_stream(requirement, path.join('spend', 'config', 'logging.ini')))
    user_conf = ConfigObj(path.join('/home/spend/.spend', filename),
                          configspec=resource_stream(requirement, path.join('spend', 'config', 'configspec.ini')))

    # _conf.merge(logging_conf)
    _conf.merge(user_conf)

    # Validate loaded config
    val = Validator()
    test = _conf.validate(val)

    if not test:
        raise RuntimeError('Problem in config file')

    return _conf


def umount_devices(devices):
    """
    Unmount devices with pumount

    Parameters
    ----------
    devices: List[str] List of strings understood by pumount as a device

    Returns
    -------

    """
    # noinspection PyUnresolvedReferences
    from sh import pumount
    from spend.Device import Device

    # TODO: show unplug message ?
    for d in devices:
        logging.debug("Unmounting device : %s", str(d))
        arg = d.device_node if isinstance(d, Device) else d

        try:
            ret = pumount(arg)
            logging.debug('Device %s unmounted (returned : %s)', arg, str(ret))
        except Exception as e:
            raise e


def toggle_state(_file):
    """Switch a file content from one to zero or inverse
    Make use of a Lock

    Parameters
    ----------
    file: str File path

    Returns
    -------
    bool New value
    """
    import fcntl

    current_state = read_file_as_bool(_file)
    with open(_file, 'w+') as fd:
        fcntl.flock(fd, fcntl.LOCK_EX)
        fd.seek(0)
        fd.write('0' if current_state else '1')
        fcntl.flock(fd, fcntl.LOCK_UN)
        fd.close()

        return not current_state


def debounce(s):
    """Decorator ensures function can only be called once every `s` seconds.
    """
    import time

    def decorate(f):
        t = None

        def wrapped(*args, **kwargs):
            nonlocal t
            t_ = time.time()
            if t is None or t_ - t >= s:
                result = f(*args, **kwargs)
                t = time.time()
                return result

        return wrapped

    return decorate


def read_file_as_bool(filepath):
    encoded_content = read_file_content(filepath).encode()
    return encoded_content == '1'.encode() or encoded_content == '1\n'.encode()


def read_file_content(filepath):
    """
    Lock the file and returns its content
    Parameters
    ----------
    filepath: str File path to read

    Returns
    -------
    str

    Raises
    ------
    IOError:
        Lock cannot be obtained
    """
    content = None

    with open(filepath, 'r') as fd:
        # Try to acquire a lock
        fcntl.flock(fd, fcntl.LOCK_EX | fcntl.LOCK_NB)
        content = fd.read()

        # Release lock and close file descriptor
        fcntl.flock(fd, fcntl.LOCK_UN)
        fd.close()

    return content


def get_resource_requirement() -> Requirement:
    return Requirement.parse("spend==%s" % __version__)


def get_child_processes(parent_id):
    from sh import ps
    ps_command = ps('--ppid', parent_id, '-o', 'pid', '--no-headers')
    ps_output = ps_command.stdout.decode('utf_8')

    return ps_output.strip().split('\n')
