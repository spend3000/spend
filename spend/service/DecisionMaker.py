# -*- coding: utf-8 -*-

import logging
from time import sleep

from spend.Device import Device
from spend.display.Message import Message
from spend.exceptions import DeviceNotWritableException, SameDeviceAndNoEncryptionException, \
    MoreThanTwoDevicesException, TargetOnBothSidesException
from spend.exceptions import NoDeviceFoundException
from spend.exceptions import NoTargetsFoundException
from spend.exceptions import NotEnoughtSpaceException


class DecisionMaker:
    """
    Choose what do to with available devices.
    Choose where to create volume and where to take files from.

    @todo: Give priority to sd cards
    """

    def choose(self, devices, is_encryption_active):
        """Choose which device to use as input, and which one as output

        Parameters
        ----------
        is_encryption_active
        devices: List[Device]

        Returns
        -------
        {'input_device':Device, 'output_device':Device]
        """
        logging.debug("Starting choice procedure")

        if len(devices) > 2:
            raise MoreThanTwoDevicesException()
        if not devices:
            raise NoDeviceFoundException()

        inp = self._choose_input_device(devices)
        out = self._choose_output_device(devices, inp)

        if inp == out and not is_encryption_active:
            raise SameDeviceAndNoEncryptionException()

        return {'input_device': inp, 'output_device': out}

    @staticmethod
    def _choose_input_device(devices):
        """Choose device used as the file source.
        choice based on 'targets' property in Device

        Parameters
        ----------
        devices: List[Device]

        Returns
        -------
        Device

        Raises
        ------
        NoTargetsFoundException
            No devices with interesting data
        MoreThanTwoDevicesException
            2 devices with interesting targets
        """
        candidates = [dict(dev=c, score=0)
                      for c in filter(lambda d: bool(len(d.metadata['targets'])), devices)]

        if not candidates:
            raise NoTargetsFoundException()

        if len(candidates) == 1:
            return candidates[0]['dev']

        # potential targets exists in both devices
        # Have a look at :
        # - sd first ?
        # - number of file ?
        # - writing date ?
        # - custom config :
        raise TargetOnBothSidesException()

    @staticmethod
    def _choose_output_device(devices, input_device):
        """Choose device used as file output (can be the input device)
        Lookup based on :
        - Available space
        - Is writable

        Parameters
        ----------
        devices: List[Device]
        input_device: Device

        Returns
        -------
        Device

        Raises
        ------
        DeviceNotWritableException
            No writable device found
        NotEnoughtSpaceException
            Not enought space
        """
        logging.debug('Choosing output device')
        logging.debug('Candidates : %s' % ', '.join(map(str, devices)))

        def filter_space(dev):
            res = input_device.metadata['targets_size'] < dev.metadata['available_space']
            if not res:
                logging.warning(
                    Message(['Device %s have not enought space' % dev.get('ID_MODEL')], duration=3,
                            level=Message.LEVEL_WARNING))
                logging.debug('Filtering device %s based on its available space' % dev)

                # Do not continue process while message is displayed
                sleep(3)

            return res

        def filter_writable(dev):
            res = bool(dev.metadata['writable'])
            if not res:
                logging.warning(
                    Message(['Device %s is read only' % dev.get('ID_MODEL')], duration=3, level=Message.LEVEL_WARNING))
                logging.debug('Filtering device %s because it is flagged as unwritable' % dev)
            return res

        candidates = list(filter(filter_writable, devices))
        if not candidates:
            raise DeviceNotWritableException("No writable device available")

        candidates = [dict(dev=c, score=0)
                      for c in filter(filter_space, candidates)]
        if not candidates:
            # NOTE: Some special cases should be created to handle the case
            # where content+Encryptedcontent > avspace > content size
            raise NotEnoughtSpaceException()

        # Now we have devices writable & with enought available space

        # Attribute a score to each device
        for c in candidates:
            logging.debug('Attributing score to device %s' % c)
            if not bool(c['dev'].metadata['targets']):
                c['score'] += 0.5
            # Do other tests

        sorted_candidates = sorted(candidates, key=lambda c: c['score'])

        logging.debug('Output selection done. Final list is : \n%s' % "\n".join(
            ['%s : %s' % (c['score'], c['dev']) for c in sorted_candidates]))

        return sorted_candidates.pop()['dev']
