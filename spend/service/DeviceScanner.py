# -*- coding: utf-8 -*-

import logging
from time import sleep
from typing import List

from pyudev import Device as UdevDevice, Context, Monitor

from spend.Device import Device
from spend.display.Message import Message
from spend.exceptions import DeviceNotMountedException
from spend.exceptions import GhostMountpointException
from spend.helpers import umount_devices
from spend.service.DeviceInspector import DeviceInspector


class DeviceScanner:
    """Tool used for device detection. Uses pyudev."""

    CONF_SECTION = 'DEVICES'
    _timeout_started = False

    @staticmethod
    def _devtype_filter(dev):
        """
        Check if dev.get('DEVTYPE') is one of allowed type

        Parameters
        ----------
        dev : UdevDevice

        Returns
        ----------
        boolean
        """
        expected = ['partition', 'usb_device', 'disk']

        if dev.get('DEVTYPE') not in expected:
            logging.debug('Filtering device {device} : DEVTYPE is {devtype}, was expecting {expected}', device=dev,
                          devtype=dev.get('DEVTYPE'), expected=', '.join(expected))
            return False

        return True

    def __init__(self, config, inspector):
        """Store provided args and config values in the object"""
        self.conf = config
        self.inspector = inspector

        self.udev_context_filters = self.conf['UDEV_CONTEXT_FILTERS'].dict().items()

    def run(self):
        """Run device detection."""

        logging.info("Start DeviceScanner run")
        devices = self.get_devices()

        if devices:
            # At least one interesting device found
            return self._init_timeout()

        # No interesting devices found
        return self._monitor()

    def get_devices_list(self):
        """
        Calls self.get_devices('list')

        Returns
        ----------
        List[Device]
        """
        return self.get_devices('list')

    def get_devices(self, returned='count'):
        """
        Return filtered devices list or count, using pyudev

        Parameters
        ----------
        returned : str
            Tell if a device list or count is expected

        Returns
        ----------
        Union[int, List[Device]]
        """
        logging.info(Message(['Scanning devices']))

        context = Context()
        udev_devices = context.list_devices(**dict(self.udev_context_filters))

        # logging.debug(
        #     '[get_devices] Context search called with options {options} returned {count} devices\nThey will be filtered and inspected..'.format(
        #         options=self.udev_context_filters, count=len(udev_devices_array)))

        # Filter on mountpoint behavior
        def filter_mountpoint(d):
            logging.debug('- %s', str(d.device_node))
            try:
                self.inspector.check(DeviceInspector.get_mount_point(d.device_node))
                return True

            except GhostMountpointException:
                logging.debug("Ghost mount point %s detected. Gonna remove it", str(d.device_node))
                umount_devices([d.device_node])

            except DeviceNotMountedException as e:
                logging.debug(
                    "Device {node} not mounted. Received : \n{error}".format(node=str(d.device_node), error=e))

            except Exception as e:
                logging.debug(
                    "DeviceInspector raise an error...: %s. Catch : %s", str(d.device_node), str(e))
                logging.error(e)
                raise e

            return False

        # Filter on udev DEVTYPE
        udev_devices_filtered = filter(self._devtype_filter, udev_devices)
        # Filter on mountpoint behavior
        udev_devices_filtered = filter(filter_mountpoint, udev_devices_filtered)

        devices = [Device(context, d, DeviceInspector.get_mount_point(d.device_node)) for d in udev_devices_filtered]

        logging.debug(devices)

        if 'count' == returned:
            return len(devices)

        return devices

    def _monitor(self):
        """
        Start udev Monitor.
        Start timeout on 'add' event from a device matching allowed DEVTYPE
        """
        logging.info(Message(['Waiting for devices']))

        context = Context()
        monitor = Monitor.from_netlink(context)

        # Synchronous detection
        for device in iter(monitor.poll, None):
            dev: Device = device
            # 'usb-storage' == device['ID_USB_DRIVER']
            if 'add' == device.action and self._devtype_filter(dev):
                logging.info(Message(["Device plug detected"], duration=1))
                break

        return self._init_timeout()

    def _init_timeout(self):
        """
        Sleep for x seconds before returning self.get_devices()
        """
        if self._timeout_started is False:
            self._timeout_started = True
            ts = int(self.conf[self.CONF_SECTION]['wait_for_another_device_seconds'])

            logging.info(Message(['%s seconds' % ts, 'before processing']))
            sleep(ts)

            return self.get_devices_list()

        raise RuntimeError('Timeout already started')
