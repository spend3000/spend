import logging
from _signal import SIGUSR1
from os import kill
from threading import Timer

import OPi.GPIO as GPIO

from spend.display.Message import Message
from spend.helpers import toggle_state, debounce


class ButtonManager:
    """
    This class contains everything related to buttons interaction
    """

    # Button names
    TYPE_SHRED = 'tshred'
    TYPE_CRYPT = 'tcrypt'
    TYPE_SHUTDOWN = 'tshutdown'

    # GPIO positions
    SW_SHRED = 10
    SW_CRYPT = 7
    SW_SHUTDOWN = 23

    DEBOUNCE_TIME = .75

    PINS = [SW_SHRED, SW_CRYPT, SW_SHUTDOWN]

    def __init__(self, conf):
        self.conf = conf

    @staticmethod
    def init_gpio():
        [GPIO.setup(pin, GPIO.IN) for pin in ButtonManager.PINS]

    def spawn(self, type_id, pid=None):
        """
        Add an event listener according to button type_id, using `GPIO.add_event_detect(GPIO.RISING)`
        Listeners are

        - Function toggling file content for shred and crypt buttons
        - Function sending SIGUSR1 to the main process for shutdown button (2s press)

        Parameters
        ----------
        type_id: str Button type. Compared to class constants
        pid: Optional[int] Pid of the programm that will be killed by shutdown button

        Returns
        -------

        """

        @debounce(ButtonManager.DEBOUNCE_TIME)
        def toggle_crypt(channel):
            logging.debug('toggle_crypt')
            new_value = toggle_state(self.conf['DEFAULT']['crypt_state_file'])
            logging.info(Message(['Encryption', 'on' if new_value else 'off'], template=Message.TEMPLATE_TYPE_SPLASH,
                                 duration=.75, level=Message.LEVEL_WARNING))

        @debounce(ButtonManager.DEBOUNCE_TIME)
        def toggle_shred(channel):
            logging.debug('toggle_shred')
            new_value = toggle_state(self.conf['DEFAULT']['shred_state_file'])
            logging.info(Message(['Transfer' if new_value else 'Copy', 'mode'], template=Message.TEMPLATE_TYPE_SPLASH,
                                 duration=.75, level=Message.LEVEL_WARNING))

        def shutdown(_pid):
            def check_and_kill(_pid):
                logging.debug('Check and kill called')
                if GPIO.input(ButtonManager.SW_SHUTDOWN):
                    logging.info(Message(['Shutdown'], template=Message.TEMPLATE_TYPE_SPLASH))
                    logging.debug('Sending SIGUSR1 to %s', str(_pid))
                    kill(_pid, SIGUSR1)

            @debounce(ButtonManager.DEBOUNCE_TIME)
            def _shutdown(_channel):
                logging.debug('Debounced shutdown called')
                # start a timer (user should keep pressed)
                timer_shutdown = Timer(2, check_and_kill, kwargs={'_pid': _pid})
                timer_shutdown.start()

            return _shutdown

        if type_id == ButtonManager.TYPE_CRYPT:
            target = toggle_crypt
            channel = ButtonManager.SW_CRYPT
        elif type_id == ButtonManager.TYPE_SHRED:
            target = toggle_shred
            channel = ButtonManager.SW_SHRED
        elif type_id == ButtonManager.TYPE_SHUTDOWN:
            if not pid:
                raise ValueError('Pid required when adding shutdown cb')
            channel = ButtonManager.SW_SHUTDOWN
            target = shutdown(pid)
            # target = shutdown(channel) if callback is None else callback
        else:
            raise RuntimeError('Specified button is not handled')

        GPIO.add_event_detect(channel, GPIO.RISING, callback=target)

    @staticmethod
    def clean():
        """
        Remove events on ButtonManager.PINS, and trigger GPIO cleaup
        Returns
        -------

        """
        [GPIO.remove_event_detect(channel) for channel in ButtonManager.PINS]
        GPIO.cleanup(ButtonManager.PINS)
