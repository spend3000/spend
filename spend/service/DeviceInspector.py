import logging
from os import W_OK, access, remove
from os.path import join
from subprocess import check_output
from typing import List

from spend.Device import Device
from spend.exceptions import DeviceNotMountedException
from spend.exceptions import GhostMountpointException


class DeviceInspector:
    """
    This class contains functions to investigate external drives
    """
    DEVICE_TYPE_CLUES_SD = ['ID_DRIVE_FLASH_SD', 'ID_DRIVE_FLASH_SDHC', 'ID_DRIVE_MEDIA_FLASH_SD']
    DEVICE_TYPE_CLUES_USB = []

    @staticmethod
    def check(mount_point):
        """Check if the device is what it claims to be

        Parameters
        ----------
        mount_point : str

        Returns
        -------
        bool

        Raises
        ------
        GhostMountpointException
            Device is `ghost`
        """
        logging.debug('Start device check')

        if DeviceInspector.is_ghost(mount_point):
            raise GhostMountpointException()

        return True

    @staticmethod
    def inspect(device, targets_expression):
        """Fill device's metadata property with :

        - device_type
        - writable
        - available_space
        - targets
        - targets_size

        Parameters
        ----------
        device : Device

        Returns
        -------
        {
            'device_type': str,
            'mount_point': str,
            'available_space': int,
            'targets': List[str],
            'targets_size': int,
            'writable': boolean
        }
        """
        logging.debug('Start device inspection')

        device.metadata['device_type'] = DeviceInspector.guess_device_type(device)
        device.metadata['writable'] = DeviceInspector.is_writable(device)
        device.metadata['available_space'] = DeviceInspector.get_available_space(
            device.metadata['mount_point'])
        device.metadata['targets'] = DeviceInspector.detect_targets(
            device.metadata['mount_point'], targets_expression)
        device.metadata['targets_size'] = DeviceInspector.get_targets_size(
            device.metadata['targets'])

        return device.metadata

    @staticmethod
    def get_mount_point(device_node):
        """
        Return mount_point for device [device_node] as listed in psutil.disk_partitions.
        Raise error if device is not present in list or if mount_point is not recognized as a mountpoint by ismount()

        Parameters
        ----------
        device_node : str
            pyudev.Device.device_node

        Returns
        -------
        str

        Raises
        ----------
        DeviceNotMountedException
            Device not present in psutils disk_partitions list or mountpoint not recognized by os.ismount
        """

        # check if device is listed in mounted partitions

        # dict_psutils_parts = {line.device: line.mountpoint for line in psutil.disk_partitions()}

        from sh import findmnt
        import json
        findmnt_command = findmnt('-J', '-o', 'SOURCE,TARGET', '-n', '-O', 'uid=1000')
        findmnt_response = findmnt_command.stdout

        if findmnt_response:
            json_response: list = json.loads(findmnt_response)['filesystems']

            logging.debug(json_response)

            for i, j in enumerate(json_response):
                if j.get('source') == device_node:
                    logging.debug(
                        "{node} mount_point is {mtp}".format(node=str(device_node), mtp=j.get('target')))
                    return j.get('target')

        raise DeviceNotMountedException(
            "Device %s not present in psutils disk_partitions list" % str(device_node))

    @staticmethod
    def guess_device_type(dev):
        """Tries to guess device type (SD/USB)

        Parameters
        ----------
        dev : Device

        Returns
        -------
        str
        """
        for clue in DeviceInspector.DEVICE_TYPE_CLUES_SD:
            if clue in dev.properties and dev.properties.asbool(clue):
                return Device.DEVICE_TYPE_SD

        for clue in DeviceInspector.DEVICE_TYPE_CLUES_USB:
            if clue in dev.properties and dev.properties.asbool(clue):
                return Device.DEVICE_TYPE_USB

        return Device.DEVICE_TYPE_UNKNOWN

    @staticmethod
    def is_writable(dev):
        """Checks if Device is writable

        Parameters
        ----------
        dev : Device

        Returns
        -------
        bool
        """
        logging.debug('Device write check')

        has_readonly_attribute = 'ro' in dev.attributes.available_attributes and dev.attributes.asint(
            'ro')

        if has_readonly_attribute or not access(dev.metadata['mount_point'], W_OK):
            logging.warning("Device is not writable")
            return False

        return True

    @staticmethod
    def get_available_space(mount_point):
        """Return free space on mount_point related filesystem.

        Uses shutil.disk_usage

        Parameters
        ----------
        mount_point : str

        Returns
        ----------
        float
            Free space in bytes
        """
        from shutil import disk_usage

        logging.debug('Available space check')

        du = disk_usage(mount_point)

        logging.debug("Device have an available space of : %s", du.free)
        return du.free

    @staticmethod
    def is_ghost(mount_point):
        """
        Try to detect a `ghost` mountpoint : e.g the mount dir exists but device is gone
        Detection done by trying to write a file and catching OsError No.5

        Parameters
        ----------
        mount_point : str

        Returns
        ----------
        bool
        """
        logging.debug('Ghost mount_point check')

        try:
            tmp = mount_point + '/test.ghost'
            open(tmp, mode='w')
            remove(tmp)

        except PermissionError as e:
            logging.error("Write permission denied. Catch : %s", e)

        except OSError as e:
            # I/O error
            if 5 == e.errno:
                return True

        return False

    # TODO: review file detection method : using `file` may be way too expensive.
    @staticmethod
    def detect_targets(mount_point, grep_expression):
        """Look for target files on device.
        Execute bash find command and format result.

        Excludes ```output``` folder

        Parameters
        ----------
        mount_point : str
            Device mount point

        Returns
        ----------
        List[str]
            Filename list
        """
        logging.debug("File detection on mount_point %s", mount_point)

        # Double curly braces for escaping
        # cmd = "find {!s} -type f -not -path \"{!s}\" -exec file {{}} \; \
        #     | awk -F: '{{if ($2 ~/image/) print $1 }}'"

        cmd = "find {!s} -type f -not -path \"{!s}\" -print | file -if - | grep -E \"{!s}\" | awk -F: '{{print $1}}'"

        cmd = cmd.format(mount_point, join(mount_point, 'output', '*'), grep_expression)
        logging.debug(cmd)

        ret = check_output(cmd, shell=True).decode('utf_8')
        ret = filter(None, (ret.strip().split("\n")))

        targets = [t for t in ret]

        logging.debug('%s subject files found : ', len(targets))
        logging.debug(targets)

        return targets

    # @todo: improve command ? Something pythonic probably exists
    @staticmethod
    def get_targets_size(targets):
        """Returns targets used space on device

        Parameters
        ----------
        targets : List[str]

        Returns
        -----------
        float
            Result in bytes
        """
        if not targets:
            return 0

        cmd = "ls -l \"{!s}\" | awk '{{sum+=$5;}} END {{print sum;}}'"
        cmd = cmd.format('" "'.join(targets))

        # logging.debug("Looking for targets total size (%s)", cmd)
        # logging.debug(check_output(cmd, shell=True).decode('utf_8'))

        return float(check_output(cmd, shell=True).decode('utf_8'))
