#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import logging
from threading import Thread
from time import sleep

import OPi.GPIO as GPIO

from spend.helpers import read_file_content, read_file_as_bool


class LedManager:
    """
    This class contains LED-related function : polling state file value and setting LED output accordingly
    """

    # Slept time between each poll
    REFRESH_FREQUENCY_SEC = .3

    # Physical index of the pins
    PIN_ID_CRYPT = 11
    PIN_ID_SHRED = 8

    PINS = [PIN_ID_SHRED, PIN_ID_CRYPT]

    def __init__(self, conf):
        # state file
        self.state_file_crypt = conf['DEFAULT']['crypt_state_file']
        self.state_file_shred = conf['DEFAULT']['shred_state_file']

    @staticmethod
    def init_gpio():
        try:
            [GPIO.setup(pin, GPIO.OUT) for pin in LedManager.PINS]
        except RuntimeError as e:
            logging.warning(e.__repr__())

    @staticmethod
    def read_state(_file):
        """Read state file. This file should contain 1 or 0
        """
        state = read_file_as_bool(_file)
        return GPIO.HIGH if state else GPIO.LOW

    @staticmethod
    def set_output(pin_id, value):
        # if value != GPIO.input(pin_id):
        GPIO.output(pin_id, value)

    def poll(self):
        """
        Read file value and set LED output for SHRED and CRYPT
        Returns
        -------

        """
        for (file, pin) in [(self.state_file_crypt, LedManager.PIN_ID_CRYPT),
                            (self.state_file_shred, LedManager.PIN_ID_SHRED)]:

            value = None
            i = 0
            while value is None:
                try:
                    value = self.read_state(file)
                except IOError:
                    if i > 9:
                        logging.error('Couldn\'t obtain lock after %s iterations' % str(i))
                        sleep(.75)
                        break
                    sleep(.1)
                finally:
                    i += 1

            # logging.debug('File %s returned %s', file, value)

            # Set value if file was not locked
            if value is not None:
                self.set_output(pin, value)

    @staticmethod
    def clean():
        for i in LedManager.PINS:
            LedManager.set_output(i, GPIO.LOW)
        GPIO.cleanup(LedManager.PINS)


class LedManagerThread(Thread):
    """
    Thread that call LedManager.poll every ```LedManager.REFRESH_FREQUENCY_SEC``` untill its death
    """
    def __init__(self, manager: LedManager):
        super().__init__(name='spend-ledmanager')
        self.manager = manager
        self.alive = False

    def run(self):
        """Start the thread

        Returns
        -------

        """
        logging.debug('Starting LedManager thread')
        self.alive = True

        while self.alive:
            self.manager.poll()
            sleep(LedManager.REFRESH_FREQUENCY_SEC)

    def stop(self):
        self.alive = False
        self.manager.clean()
