.. spend documentation master file, created by
   sphinx-quickstart on Fri Mar  1 12:02:08 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Spend
=================================



.. toctree::
   :hidden:

   usage/index
   source/modules

.. include:: ../README.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
