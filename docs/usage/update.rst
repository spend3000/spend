Update
======

As some software updates will probably be supplied, Spend offer a way to update easily.

You just have to put the supplied file in the SD cards, in the folder ``/home/spend/update`` ; and start Spend.
Information about update will be displayed on the screen

.. note:: additional instructions may be given altogether with the update files