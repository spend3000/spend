Encryption
==========

Spend offers encryption functionality, with veracrypt and GPG.

.. warning::
    Spend is using symetric and asymetric encryption, using veracrypt containers and PGP-protected messages.
    If you want use these features, you should learn how it works.
    Some links about that :

    * https://blog.vrypan.net/2013/08/28/public-key-cryptography-for-non-geeks/

    * https://en.wikipedia.org/wiki/Public-key_cryptography

    * https://hackernoon.com/symmetric-and-asymmetric-encryption-5122f9ec65b1


Encryption works this way :

* A random name for the run is chosen. Let's call it ``RunA``

* A random password (``KeyA``) is created. It is stored PGP-encrypted on the output device, under the name ``RunA.key``

* A veracrypt encrypted volume named ``RunA.files``, which can be open with ``KeyA``, is created on the output device

* Targets files are stored in the encrypted volume

So, **to decrypt the files :**

* Decrypt the file ``xxx.key`` with PGP, using your favorite software.

* Open the veracrypt volume, called ``xxx.files``. The password is inside the file you just opened via PGP

.. todo: list of softwares

.. _usage-workflow-encryption-update:

Importing a new public key
--------------------------

Spend have an automatic update system which lets you use your own GPG keyring to receive the files.

You just have to export your public key (cf. documentation of the software you uses), and store it in Spend's SD card at this place : ``/home/spend/update/key.gpg``
