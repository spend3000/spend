Devices
=======
Once Spend is on and running, it will triggers a device research. If no seemingly interesting device is found,
it will start monitoring devices plug.

Whenever a device is found, a timer in :ref:`usage-configuration` is set to let the user the possibility to plug another device.

.. warning::
    Only mounted devices are processed.
    That means that if the device is somehow in a abnormal state, it will not be considered.

Supported devices
-----------------

Due to its low electrical power, Spend should not be use with external hard drive that have no external power sources.
Actually, it has only be tested with

* various cheap and wide-spread USB pens

* SD cards

* µSD card