Decision
========

Once all data about device is gathered, Spend have to choose which device will be used as input and which one as output.

.. warning::
    Spend does not support use of more than two devices at the same time

.. note::
    If only one device is present, the only thing that can be done is encrypting the files and storing them on the
    same device (other things are meaningless)

Decision is taken according to this heuristic :

* If device A have interesting file and device B not, copy from A to B
* If device A and B have interesting files :

  + If *one* device is recognized as an SD card, copy from the SD to the other device

  + Else *fail*

.. note::
    It can be annoying that the output device needs to be free of files that can be considered as targets.
    However you can place them in a folder named ``output`` on the root of your removable drive.
    They will be excluded from scan
