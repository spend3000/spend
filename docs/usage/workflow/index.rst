Workflow
========

In this chapter, spend working process is succinctly described : how  it look for devices, target files on it,
how it chooses what to do, and which kind of encryption it uses

.. toctree::
   :maxdepth: 2

   devices
   targets
   decision
   encryption
