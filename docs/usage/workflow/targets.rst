Targets
=======

Once a device is detected, it is inspected to see how much space is available, and if interesting files are present.

By default, target files are images and videos only, but this can be changed in the :ref:`usage-configuration` options

.. warning::
    If you ask Spend to proceed all file types, it can quickly get stuck if the device where you want to output already contains files

If encryption is off, Spend will copy the files in a folder named ``output``

The files present in this folder are then ignored by the scans. This allows Spend to be used many with the same output
device
