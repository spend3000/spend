from os import symlink, seteuid, remove, mkdir, geteuid, chmod, environ
from os.path import join
from subprocess import call

from setuptools import setup
from setuptools.command.develop import develop
from setuptools.command.install import install

from spend.helpers import copy_files, safeutil


def _link(_from, _to):
    try:
        remove(_to)
    except FileNotFoundError:
        pass
    finally:
        symlink(_from, _to)


# @todo: Create a hook for uninstall ?

class PostDevelopCommand(develop):
    def run(self):
        if geteuid() != 0:
            print('\n\n\tERROR : Install should be run as root\n\n')
        # seteuid(1000)

        develop.run(self)

        # noinspection PyBroadException
        try:
            # noinspection PyStatementEffect
            environ["SPEND_ENV"]

            # noinspection PyBroadException
            try:
                mkdir('/var/log/spend')
            except Exception:
                pass

            create_symlinks('/home/spend/spend/')
            install_services('/home/spend/spend/')
        except Exception:
            print('SPEND_ENV environ variable not set')


class PostInstallCommand(install):
    def run(self):
        if geteuid() != 0:
            print('\n\n\tERROR : Install should be run as root\n\n')
        # seteuid(1000)

        install.run(self)

        # noinspection PyBroadException
        try:
            # noinspection PyStatementEffect
            environ["SPEND_ENV"]
            # noinspection PyBroadException
            try:
                mkdir('/var/log/spend')
            except Exception:
                pass

            print("Installation dir is : %s", self.install_lib)

            create_symlinks(self.install_lib)
            install_services(self.install_lib)
        except Exception:
            print('SPEND_ENV environ variable not set')


def create_symlinks(installation_path):
    """
    Create links to the package configuration in home & etc
    Parameters
    ----------
    installation_path

    Returns
    -------

    """
    config_dir = join(installation_path, 'spend', 'config')
    filename = 'example.ini'
    _link(join(config_dir, filename), join('/home/spend/.spend', filename))

    for _filename in ['test.ini', 'prod.ini']:
        safeutil.copyfile(join(config_dir, filename), join('/home/spend/.spend', _filename))

    seteuid(0)
    for _filename in ['configspec.ini', 'test.ini', 'prod.ini', 'logging.ini']:
        _link(join(config_dir, _filename), join('/etc/spend', _filename))

    for _filename in ['91-udev.rules', '90-usbmount.rules']:
        _link(join(config_dir, _filename), join('/etc/udev/rules.d', _filename))
        _link(join(config_dir, _filename), join('/lib/udev/rules.d', _filename))

    seteuid(1000)


def install_services(installation_path):
    """
    Create systemd target and services
    Make spend.target default

    Parameters
    ----------
    installation_path

    Returns
    -------

    """
    seteuid(0)
    config_dir = join(installation_path, 'spend', 'config')

    try:
        mkdir('/etc/systemd/system/spend.target.wants')
    except Exception:
        pass

    try:
        mkdir('/lib/systemd/system/spend.target.wants')
    except Exception:
        pass

    copy_files([join(config_dir, 'services/spend.target')], '/etc/systemd/system')
    copy_files([join(config_dir, 'services/spend-update.service'),
                join(config_dir, 'services/spend.service')],
               '/lib/systemd/system')

    _link('/lib/systemd/system/spend-update.service', '/lib/systemd/system/spend.target.wants/spend-update.service')
    _link('/lib/systemd/system/spend.service', '/lib/systemd/system/spend.target.wants/spend.service')

    _link('/lib/systemd/system/spend.target.wants/spend-update.service',
          '/etc/systemd/system/spend.target.wants/spend-update.service')
    _link('/lib/systemd/system/spend.target.wants/spend.service',
          '/etc/systemd/system/spend.target.wants/spend.service')

    chmod('/lib/systemd/system/spend-update.service', 0o664)
    chmod('/lib/systemd/system/spend.service', 0o664)
    chmod('/etc/systemd/system/spend.target', 0o664)

    call(['/bin/systemctl', 'enable', 'spend.target'])
    call(['/bin/systemctl', 'set-default', 'spend.target'])

    seteuid(1000)


# seteuid(1000)
setup_returned = setup(
    cmdclass={
        'develop': PostDevelopCommand,
        'install': PostInstallCommand,
    },
)
